<?php get_header(); ?>

	<main id="content" class="row" role="main">

		<article id="content-not-found" class="columns large-8">
		
			<header class="article-header">
				<h1><?php _e( 'Epic 404 - Article Not Found', 'elami' ); ?></h1>
			</header> <!-- end article header -->
	
			<section class="entry-content">
				<p><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'elami' ); ?></p>
			</section> <!-- end article section -->

			<section class="search">
			    <p><?php get_search_form(); ?></p>
			</section> <!-- end search section -->
	
		</article> <!-- end article -->

	</main> <!-- end #content -->

<?php get_footer(); ?>