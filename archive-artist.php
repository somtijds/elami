<?php get_header(); ?>
			
<main id="content" role="main">
	
	<?php if (have_posts()) : ?>

		<div class="row">

		<?php while (have_posts()) : the_post(); ?>

			<?php get_template_part( 'parts/loop', 'archive-artist' ); ?>

		<?php endwhile; ?>	

		</div>

		<?php elami_page_navi(); ?>
		
	<?php else : ?>
								
		<?php get_template_part( 'parts/content', 'missing' ); ?>
			
	<?php endif; ?>
	
</main> <!-- end #main -->

<?php get_footer(); ?>