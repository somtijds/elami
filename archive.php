<?php get_header(); ?>
	
	<?php 
		global $wp_query;
		$paged_class = $wp_query->max_num_pages > 1 ? ' paged' : ''; ?>

	<main id="content" class="grid<?php echo esc_attr( $paged_class ); ?>" role="main">

	<?php if (have_posts()) : ?>
		
		<div class="row" data-equalizer data-equalize-by-row="false">
		
		<?php while (have_posts()) : the_post(); ?>

			<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>

		<?php endwhile; ?>	

		</div>

		<?php elami_page_navi(); ?>

	<?php else : ?>

								
		<?php get_template_part( 'parts/content', 'missing' ); ?>
			
	<?php endif; ?>

</main> <!-- end #main -->

<?php get_footer(); ?>