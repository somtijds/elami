<?php 
/**
 * Filters and functions related to the Breadcrumb Trail plugin.
 *
 * @package elami
 **/

add_filter( 'breadcrumb_trail','elami_breadcrumb_trail' );

/**
 * Filter breadcrumb trail output
 *
 * @return breadcrumb trail
 * @author 
 **/
function elami_breadcrumb_trail( $html ) {
	$html = str_replace( 'itemtype="http://schema.org/ListItem" class="trail-item', 'itemtype="http://schema.org/ListItem"class="button trail-item', $html );

	if ( is_tax( 'publisher' ) ) {
		$html = str_replace( 'trail-end">', 'trail-end">' . esc_html__('Publisher: ','elami'), $html );
	}
	
	if ( is_post_type_archive( 'book' ) && strpos( $html,'<span itemprop="name">Books</span>' ) !== false ) {
		$html = str_replace( '<span itemprop="name">Books</span>', 
			'<a href="' . get_post_type_archive_link( 'book' ) . '"><span itemprop="name">Books</span></a>', 
			$html );
	}
	
	return $html;
}

add_filter( 'breadcrumb_trail_object','elami_breadcrumb_trail_object' );

/**
 * Filter breadcrumb trail object
 *
 * @return breadcrumb trail
 * @author 
 **/
function elami_breadcrumb_trail_object ( $bct ) {
	return $bct;
}

add_filter( 'breadcrumb_trail_items','elami_breadcrumb_trail_items' );

/**
 * Filter breadcrumb trail object
 *
 * @return breadcrumb trail
 * @author 
 **/
function elami_breadcrumb_trail_items ( $items ) {

	if ( empty( $items ) ) {
		return $items;
	}
	$new_items = array();

	foreach( $items as $item ) {
		if ( ! strpos( $item, 'rel="home"' ) ) {
			$new_items[] = $item;
		}

	}

	// Add artist name in book archive pages.
	if ( is_post_type_archive( 'book' ) ) {
		$artist_id = get_query_var( 'artist_id' );
		// Verify that the post exists.
		$artist = elami_artist_exists( $artist_id );	
		if (  $artist ) {
			$new_items[] = '<span>' . esc_html__( 'Artist: ', 'elami' ) . $artist->post_title . '</span>';
		}
	}

	return $new_items;
}