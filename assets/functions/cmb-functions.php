<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'elami_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category elami
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 object $cmb CMB2 object
 *
 * @return bool             True if metabox should show
 */
function elami_show_if_front_page( $cmb ) {
	// Don't show this metabox if it's not the front page template
	if ( $cmb->object_id !== get_option( 'page_on_front' ) ) {
		return false;
	}
	return true;
}

/**
 *
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
function elami_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

/**
 * Manually render a field column display.
 *
 * @param  array      $field_args Array of field arguments.
 * @param  CMB2_Field $field      The field object
 */
function elami_display_text_small_column( $field_args, $field ) {
	?>
	<div class="custom-column-display <?php echo $field->row_classes(); ?>">
		<p><?php echo $field->escaped_value(); ?></p>
		<p class="description"><?php echo $field->args( 'description' ); ?></p>
	</div>
	<?php
}

add_action( 'cmb2_admin_init', 'elami_register_about_page_metabox' );
/**
 * Hook in and add a metabox that only appears on the 'About' page
 */
function elami_register_about_page_metabox() {
	$prefix = 'elami_';

	/**
	 * Metabox to be displayed on a single page ID
	 */
	$cmb_about_page = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Page subtitle', 'elami' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'id' => array( 2,20,22,24 ) ), // Specific post IDs to display this metabox
	) );

	$cmb_about_page->add_field( array(
		'name' => __( 'Add a subtitle for this page', 'elami' ),
		'id'   => $prefix . 'page_subtitle',
		'type' => 'text',
	) );

}

add_action( 'cmb2_admin_init', 'elami_register_artist_management_metabox' );
/**
 * Hook in and add a metabox that only appears on the 'About' page
 */
function elami_register_artist_management_metabox() {
	$prefix = 'elami_artist_';

	/**
	 * Metabox to be displayed on a single page ID
	 */
	$cmb_artist_management_metabox = new_cmb2_box( array(
		'id'           => $prefix . 'management_metabox',
		'title'        => __( 'Artist Management by élami?', 'elami' ),
		'object_types' => array( 'artist', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cmb_artist_management_metabox->add_field( array(
		'name' => __( 'Of course!', 'elami' ),
		'description' => __( 'Check this box if this artist is (exclusively) managed by élami', 'elami' ),
		'id'   => $prefix . 'management',
		'type' => 'checkbox',
	) );

}

add_action( 'cmb2_admin_init', 'elami_register_client_group_metabox' );
/**
 * Hook in and add a repeatable metabox to add client details
 */
function elami_register_client_group_metabox() {
	$prefix = 'elami_group_';

	/**
	 * Repeatable Field Groups
	 */
	$client_group = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Clients', 'elami' ),
		'object_types' => array( 'page', ),
		'show_on'	   => array( 'id' => array( 2,20,22,24 ) ),
	) );

	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
	$group_field_id = $client_group->add_field( array(
		'id'          => $prefix . 'clients',
		'type'        => 'group',
		'description' => __( 'Generates blocks with (corporate) client info', 'elami' ),
		'options'     => array(
			'group_title'   => __( 'Client {#}', 'elami' ), // {#} gets replaced by row number
			'add_button'    => __( 'Add Another Client', 'elami' ),
			'remove_button' => __( 'Remove Client', 'elami' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );

	/**
	 * Group fields works the same, except ids only need
	 * to be unique to the group. Prefix is not needed.
	 *
	 * The parent field's id needs to be passed as the first argument.
	 */
	$client_group->add_group_field( $group_field_id, array(
		'name'       => __( 'Client Name', 'elami' ),
		'id'         => 'client_name',
		'type'       => 'text',
	) );

	$client_group->add_group_field( $group_field_id, array(
		'name' => __( 'Client type', 'elami' ),
		'description' => __( 'For example:        Publishing house', 'elami' ),
		'id'   => 'client_type',
		'type' => 'text',
	) );

	$client_group->add_group_field( $group_field_id, array(
		'name'        => __( 'Client Description', 'elami' ),
		'description' => __( 'Write a short description for this client', 'elami' ),
		'id'          => 'client_description',
		'type'        => 'textarea_small',
	) );

	$client_group->add_group_field( $group_field_id, array(
		'name' => __( 'Client Image', 'elami' ),
		'id'   => 'client_image',
		'type' => 'file',
	) );


	$client_group->add_group_field( $group_field_id, array(
		'name' => __( 'Client Website Link', 'elami' ),
		'id'   => 'client_url',
		'type' => 'text_url',
	) );

	$client_group->add_group_field( $group_field_id, array(
		'name'     => __( 'Client Tag (to display books)', 'elami' ),
		'desc'     => __( 'Select the tag here to connect books to this client', 'elami' ),
		'id'       => 'client_taxonomy_select',
		'type'     => 'taxonomy_select',
		'taxonomy' => 'publisher', // Taxonomy Slug
	) );

	// Via: https://github.com/WebDevStudios/CMB2/wiki/Tips-&-Tricks#a-dropdown-for-taxonomy-terms-which-does-not-set-the-term-on-the-post
	$client_group->add_group_field( $group_field_id, array(
		'name'     => __( 'Client Tag (to display books)', 'elami' ),
		'desc'     => __( 'Select the tag here to connect books to this client', 'elami' ),
		'id'       => 'client_taxonomy_select',
    	'type'           => 'select',
	    // Use a callback to avoid performance hits on pages where this field is not displayed (including the front-end).
	    'options_cb'     => 'elami_get_client_term_options',
	    // Same arguments you would pass to `get_terms`.
	    'get_terms_args' => array(
	        'taxonomy'   => 'publisher',
	        'hide_empty' => false,
	    ),
) );

}
add_action( 'cmb2_admin_init', 'elami_register_book_subtitle_and_artist_metabox' );
/**
 * Hook in and add a metabox that only appears on the 'About' page
 */
function elami_register_book_subtitle_and_artist_metabox() {
	$prefix = 'elami_book_';

	/**
	 * Metabox to be displayed on a single page ID
	 */
	$cmb_title = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Subtitle and Artist(s)', 'elami' ),
		'object_types' => array( 'book', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => false, // Show field names on the left
	) );

	$cmb_title->add_field( array(
		'name' => __( 'Add a subtitle for this book', 'elami' ),
		'id'   => $prefix . 'subtitle',
		'type' => 'text',
	) );

	$cmb_title->add_field( array(
		'name'    => __( 'Attached Author', 'elami' ),
		'desc'    => __( 'Drag posts from the left column to the right column to attach them to this book.<br />You may rearrange the order of the posts in the right column by dragging and dropping.', 'elami' ),
		'id'      => $prefix . 'attached_authors',
		'type'    => 'custom_attached_posts',
		'options' => array(
			'show_thumbnails' => true, // Show thumbnails on the left
			'filter_boxes'    => false, // Show a text box for filtering the results
			'query_args'      => array(
				'posts_per_page' => 10,
				'post_type'      => 'artist',
			), // override the get_posts args
		),
	) );

	$cmb_title->add_field( array(
		'name'    => __( 'Attached Illustrator', 'elami' ),
		'desc'    => __( 'Drag posts from the left column to the right column to attach them to this book.<br />You may rearrange the order of the posts in the right column by dragging and dropping.', 'elami' ),
		'id'      => $prefix . 'attached_illustrators',
		'type'    => 'custom_attached_posts',
		'options' => array(
			'show_thumbnails' => true, // Show thumbnails on the left
			'filter_boxes'    => false, // Show a text box for filtering the results
			'query_args'      => array(
				'posts_per_page' => 10,
				'post_type'      => 'artist',
			), // override the get_posts args
		),
	) );
}

add_action( 'cmb2_admin_init', 'elami_register_book_details_metabox' );
/**
 * Hook in and add a metabox to add technical details to a title
 */
function elami_register_book_details_metabox() {
	$prefix = 'elami_book_details_';

	/**
	 * Metabox to be displayed on a single title
	 */
	$book_details = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Technical details', 'elami' ),
		'object_types' => array( 'book', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$book_details->add_field( array(
		'name' => __( 'Original Title', 'elami' ),
		'id'   => $prefix . 'original_title',
		'type' => 'text',
	) );

	// $book_details->add_field( array(
	// 	'name' => __( 'Format', 'elami' ),
	// 	'id'   => $prefix . 'format',
	// 	'type' => 'text',
	// ) );

	$book_details->add_field( array(
		'name'           => 'Format',
		'id'   			 => $prefix . 'format',
		'taxonomy'       => 'format', // Enter Taxonomy Slug
		'type'           => 'taxonomy_radio',
		// Optional :
		'text'           => array(
			'no_terms_text' => 'Sorry, no terms could be found.' // Change default text. Default: "No terms"
		),
		'remove_default' => 'true' // Removes the default metabox provided by WP core. Pending release as of Aug-10-16
	) );	

	$book_details->add_field( array(
		'name' => __( 'Pages', 'elami' ),
		'id'   => $prefix . 'pages',
		'type' => 'text',
	) );

	$book_details->add_field( array(
		'name' => __( 'Size(s)', 'elami' ),
		'id'   => $prefix . 'size',
		'type' => 'text',
	) );

}

add_action( 'cmb2_admin_init', 'elami_register_book_rights_metabox' );
/**
 * Hook in and add a metabox to add rights per country
 */
function elami_register_book_rights_metabox() {
	$prefix = 'elami_book_rights_sold_';

	/**
	 * Repeatable Field Groups
	 */
	$rights_group = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Rights sold', 'elami' ),
		'object_types' => array( 'book', ),
	) );

	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
	$group_field_id = $rights_group->add_field( array(
		'id'          => $prefix . 'countries',
		'type'        => 'group',
		'description' => __( 'Generates blocks with rights info per publisher', 'elami' ),
		'options'     => array(
			'group_title'   => __( 'Publisher {#}', 'elami' ), // {#} gets replaced by row number
			'add_button'    => __( 'Add Another Publisher', 'elami' ),
			'remove_button' => __( 'Remove Publisher', 'elami' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );

	// Plugin-dependent multi-select:
	if ( class_exists( 'PW_CMB2_Field_Select2' ) ) {
		$language_select_type = 'pw_multiselect';
		//$country_select_type = 'pw_select';
	} else {
		$language_select_type = 'select';
		//$country_select_type = 'select';
	}

	$rights_group->add_group_field( $group_field_id, array(
		'name'       => __( 'Publisher', 'elami' ),
		'id'         => 'rights_sold_publisher',
		'type'       => 'text',
	) );

	// $rights_group->add_group_field( $group_field_id, array(
	// 	'name'    => __( 'Country', 'elami' ),
	// 	'desc'    => __( 'Select a country', 'elami' ),
	// 	'id'      => 'rights_sold_country',
	// 	'type'    => $country_select_type,
	// 	'options' => elami_country_list(),
	// ) );

	$rights_group->add_group_field( $group_field_id, array(
		'name'    => __( 'Languages', 'elami' ),
		'desc'    => __( 'Choose one or more languages', 'elami' ),
		'id'      => 'rights_sold_languages',
		'type'    => $language_select_type,
		'options' => elami_language_list(),
	) );

	$rights_group->add_group_field( $group_field_id, array(
		'name' => __( 'Other info', 'elami' ),
		'id'   => 'rights_sold_other_info',
		'desc'    => __( 'This information will be added in parentheses behind the selected languages.', 'elami' ),
		'type' => 'text',
	) );



}

/**
 * Gets a number of terms and displays them as options
 * @param  CMB2_Field $field 
 * @return array An array of options that matches the CMB2 options array
 */
function elami_get_client_term_options( $field ) {
	$args = $field->args( 'get_terms_args' );
	$args = is_array( $args ) ? $args : array();

	$args = wp_parse_args( $args, array( 'taxonomy' => 'publisher' ) );

	$taxonomy = $args['taxonomy'];

	$terms = (array) cmb2_utils()->wp_at_least( '4.5.0' )
		? get_terms( $args )
		: get_terms( $taxonomy, $args );

	// Initate an array with an empty value.
	$term_options = array( __( "Pick a tag or create one when it doesn't exist", 'elami' ) );
	if ( ! empty( $terms ) ) {
		foreach ( $terms as $term ) {
			$term_options[ $term->term_id ] = $term->name;
		}
	}

	return $term_options;
}

add_action( 'cmb2_admin_init', 'elami_register_theme_options_metabox' );
/**
 * Hook in and register a metabox to handle a theme options page
 */
function elami_register_theme_options_metabox() {

	$option_key = 'elami_theme_options';

	/**
	 * Metabox for an options page. Will not be added automatically, but needs to be called with
	 * the `cmb2_metabox_form` helper function. See wiki for more info.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'      => $option_key . 'page',
		'title'   => __( 'Theme Options Metabox', 'elami' ),
		'hookup'  => false, // Do not need the normal user/post hookup
		'show_on' => array(
			// These are important, don't remove
			'key'   => 'options-page',
			'value' => array( $option_key )
		),
	) );

	/**
	 * Options fields ids only need
	 * to be unique within this option group.
	 * Prefix is not needed.
	 */
	$cmb_options->add_field( array(
		'name'    => __( 'Site Background Color', 'elami' ),
		'desc'    => __( 'field description (optional)', 'elami' ),
		'id'      => 'bg_color',
		'type'    => 'colorpicker',
		'default' => '#ffffff',
	) );

}


// add_action( 'cmb2_admin_init', 'elami_register_demo_metabox' );
// /**
//  * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
//  */
// function elami_register_demo_metabox() {
// 	$prefix = 'elami_demo_';
//
// 	/**
// 	 * Sample metabox to demonstrate each field type included
// 	 */
// 	$cmb_demo = new_cmb2_box( array(
// 		'id'            => $prefix . 'metabox',
// 		'title'         => __( 'Subtitle', 'elami' ),
// 		'object_types'  => array( 'page', ), // Post type
// 		// 'show_on_cb' => 'elami_show_if_front_page', // function should return a bool value
// 		// 'context'    => 'normal',
// 		// 'priority'   => 'high',
// 		// 'show_names' => true, // Show field names on the left
// 		// 'cmb_styles' => false, // false to disable the CMB stylesheet
// 		// 'closed'     => true, // true to keep the metabox closed by default
// 		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
// 		// 'classes_cb' => 'elami_add_some_classes', // Add classes through a callback.
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'       => __( 'Test Text', 'elami' ),
// 		'desc'       => __( 'field description (optional)', 'elami' ),
// 		'id'         => $prefix . 'text',
// 		'type'       => 'text',
// 		'show_on_cb' => 'elami_hide_if_no_cats', // function should return a bool value
// 		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
// 		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
// 		// 'on_front'        => false, // Optionally designate a field to wp-admin only
// 		// 'repeatable'      => true,
// 		// 'column'          => true, // Display field value in the admin post-listing columns
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Text Small', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'textsmall',
// 		'type' => 'text_small',
// 		// 'repeatable' => true,
// 		// 'column' => array(
// 		// 	'name'     => __( 'Column Title', 'elami' ), // Set the admin column title
// 		// 	'position' => 2, // Set as the second column.
// 		// );
// 		// 'display_cb' => 'elami_display_text_small_column', // Output the display of the column values through a callback.
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Text Medium', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'textmedium',
// 		'type' => 'text_medium',
// 		// 'repeatable' => true,
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Custom Rendered Field', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'render_row_cb',
// 		'type' => 'text',
// 		'render_row_cb' => 'elami_render_row_cb',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Website URL', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'url',
// 		'type' => 'text_url',
// 		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
// 		// 'repeatable' => true,
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Text Email', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'email',
// 		'type' => 'text_email',
// 		// 'repeatable' => true,
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Time', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'time',
// 		'type' => 'text_time',
// 		// 'time_format' => 'H:i', // Set to 24hr format
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Time zone', 'elami' ),
// 		'desc' => __( 'Time zone', 'elami' ),
// 		'id'   => $prefix . 'timezone',
// 		'type' => 'select_timezone',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Date Picker', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'textdate',
// 		'type' => 'text_date',
// 		// 'date_format' => 'Y-m-d',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Date Picker (UNIX timestamp)', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'textdate_timestamp',
// 		'type' => 'text_date_timestamp',
// 		// 'timezone_meta_key' => $prefix . 'timezone', // Optionally make this field honor the timezone selected in the select_timezone specified above
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Date/Time Picker Combo (UNIX timestamp)', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'datetime_timestamp',
// 		'type' => 'text_datetime_timestamp',
// 	) );
//
// 	// This text_datetime_timestamp_timezone field type
// 	// is only compatible with PHP versions 5.3 or above.
// 	// Feel free to uncomment and use if your server meets the requirement
// 	// $cmb_demo->add_field( array(
// 	// 	'name' => __( 'Test Date/Time Picker/Time zone Combo (serialized DateTime object)', 'elami' ),
// 	// 	'desc' => __( 'field description (optional)', 'elami' ),
// 	// 	'id'   => $prefix . 'datetime_timestamp_timezone',
// 	// 	'type' => 'text_datetime_timestamp_timezone',
// 	// ) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Money', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'textmoney',
// 		'type' => 'text_money',
// 		// 'before_field' => '£', // override '$' symbol if needed
// 		// 'repeatable' => true,
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'    => __( 'Test Color Picker', 'elami' ),
// 		'desc'    => __( 'field description (optional)', 'elami' ),
// 		'id'      => $prefix . 'colorpicker',
// 		'type'    => 'colorpicker',
// 		'default' => '#ffffff',
// 		// 'attributes' => array(
// 		// 	'data-colorpicker' => json_encode( array(
// 		// 		'palettes' => array( '#3dd0cc', '#ff834c', '#4fa2c0', '#0bc991', ),
// 		// 	) ),
// 		// ),
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Text Area', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'textarea',
// 		'type' => 'textarea',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Text Area Small', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'textareasmall',
// 		'type' => 'textarea_small',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Text Area for Code', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'textarea_code',
// 		'type' => 'textarea_code',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Title Weeeee', 'elami' ),
// 		'desc' => __( 'This is a title description', 'elami' ),
// 		'id'   => $prefix . 'title',
// 		'type' => 'title',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'             => __( 'Test Select', 'elami' ),
// 		'desc'             => __( 'field description (optional)', 'elami' ),
// 		'id'               => $prefix . 'select',
// 		'type'             => 'select',
// 		'show_option_none' => true,
// 		'options'          => array(
// 			'standard' => __( 'Option One', 'elami' ),
// 			'custom'   => __( 'Option Two', 'elami' ),
// 			'none'     => __( 'Option Three', 'elami' ),
// 		),
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'             => __( 'Test Radio inline', 'elami' ),
// 		'desc'             => __( 'field description (optional)', 'elami' ),
// 		'id'               => $prefix . 'radio_inline',
// 		'type'             => 'radio_inline',
// 		'show_option_none' => 'No Selection',
// 		'options'          => array(
// 			'standard' => __( 'Option One', 'elami' ),
// 			'custom'   => __( 'Option Two', 'elami' ),
// 			'none'     => __( 'Option Three', 'elami' ),
// 		),
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'    => __( 'Test Radio', 'elami' ),
// 		'desc'    => __( 'field description (optional)', 'elami' ),
// 		'id'      => $prefix . 'radio',
// 		'type'    => 'radio',
// 		'options' => array(
// 			'option1' => __( 'Option One', 'elami' ),
// 			'option2' => __( 'Option Two', 'elami' ),
// 			'option3' => __( 'Option Three', 'elami' ),
// 		),
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'     => __( 'Test Taxonomy Radio', 'elami' ),
// 		'desc'     => __( 'field description (optional)', 'elami' ),
// 		'id'       => $prefix . 'text_taxonomy_radio',
// 		'type'     => 'taxonomy_radio',
// 		'taxonomy' => 'category', // Taxonomy Slug
// 		// 'inline'  => true, // Toggles display to inline
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'     => __( 'Test Taxonomy Select', 'elami' ),
// 		'desc'     => __( 'field description (optional)', 'elami' ),
// 		'id'       => $prefix . 'taxonomy_select',
// 		'type'     => 'taxonomy_select',
// 		'taxonomy' => 'category', // Taxonomy Slug
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'     => __( 'Test Taxonomy Multi Checkbox', 'elami' ),
// 		'desc'     => __( 'field description (optional)', 'elami' ),
// 		'id'       => $prefix . 'multitaxonomy',
// 		'type'     => 'taxonomy_multicheck',
// 		'taxonomy' => 'post_tag', // Taxonomy Slug
// 		// 'inline'  => true, // Toggles display to inline
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Checkbox', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'checkbox',
// 		'type' => 'checkbox',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'    => __( 'Test Multi Checkbox', 'elami' ),
// 		'desc'    => __( 'field description (optional)', 'elami' ),
// 		'id'      => $prefix . 'multicheckbox',
// 		'type'    => 'multicheck',
// 		// 'multiple' => true, // Store values in individual rows
// 		'options' => array(
// 			'check1' => __( 'Check One', 'elami' ),
// 			'check2' => __( 'Check Two', 'elami' ),
// 			'check3' => __( 'Check Three', 'elami' ),
// 		),
// 		// 'inline'  => true, // Toggles display to inline
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'    => __( 'Test wysiwyg', 'elami' ),
// 		'desc'    => __( 'field description (optional)', 'elami' ),
// 		'id'      => $prefix . 'wysiwyg',
// 		'type'    => 'wysiwyg',
// 		'options' => array( 'textarea_rows' => 5, ),
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'Test Image', 'elami' ),
// 		'desc' => __( 'Upload an image or enter a URL.', 'elami' ),
// 		'id'   => $prefix . 'image',
// 		'type' => 'file',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'         => __( 'Multiple Files', 'elami' ),
// 		'desc'         => __( 'Upload or add multiple images/attachments.', 'elami' ),
// 		'id'           => $prefix . 'file_list',
// 		'type'         => 'file_list',
// 		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name' => __( 'oEmbed', 'elami' ),
// 		'desc' => __( 'Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'elami' ),
// 		'id'   => $prefix . 'embed',
// 		'type' => 'oembed',
// 	) );
//
// 	$cmb_demo->add_field( array(
// 		'name'         => 'Testing Field Parameters',
// 		'id'           => $prefix . 'parameters',
// 		'type'         => 'text',
// 		'before_row'   => 'elami_before_row_if_2', // callback
// 		'before'       => '<p>Testing <b>"before"</b> parameter</p>',
// 		'before_field' => '<p>Testing <b>"before_field"</b> parameter</p>',
// 		'after_field'  => '<p>Testing <b>"after_field"</b> parameter</p>',
// 		'after'        => '<p>Testing <b>"after"</b> parameter</p>',
// 		'after_row'    => '<p>Testing <b>"after_row"</b> parameter</p>',
// 	) );
//
// }


// add_action( 'cmb2_admin_init', 'elami_register_user_profile_metabox' );
// /**
//  * Hook in and add a metabox to add fields to the user profile pages
//  */
// function elami_register_user_profile_metabox() {
// 	$prefix = 'elami_user_';
//
// 	/**
// 	 * Metabox for the user profile screen
// 	 */
// 	$cmb_user = new_cmb2_box( array(
// 		'id'               => $prefix . 'edit',
// 		'title'            => __( 'User Profile Metabox', 'elami' ), // Doesn't output for user boxes
// 		'object_types'     => array( 'user' ), // Tells CMB2 to use user_meta vs post_meta
// 		'show_names'       => true,
// 		'new_user_section' => 'add-new-user', // where form will show on new user page. 'add-existing-user' is only other valid option.
// 	) );
//
// 	$cmb_user->add_field( array(
// 		'name'     => __( 'Extra Info', 'elami' ),
// 		'desc'     => __( 'field description (optional)', 'elami' ),
// 		'id'       => $prefix . 'extra_info',
// 		'type'     => 'title',
// 		'on_front' => false,
// 	) );
//
// 	$cmb_user->add_field( array(
// 		'name'    => __( 'Avatar', 'elami' ),
// 		'desc'    => __( 'field description (optional)', 'elami' ),
// 		'id'      => $prefix . 'avatar',
// 		'type'    => 'file',
// 	) );
//
// 	$cmb_user->add_field( array(
// 		'name' => __( 'Facebook URL', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'facebookurl',
// 		'type' => 'text_url',
// 	) );
//
// 	$cmb_user->add_field( array(
// 		'name' => __( 'Twitter URL', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'twitterurl',
// 		'type' => 'text_url',
// 	) );
//
// 	$cmb_user->add_field( array(
// 		'name' => __( 'Google+ URL', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'googleplusurl',
// 		'type' => 'text_url',
// 	) );
//
// 	$cmb_user->add_field( array(
// 		'name' => __( 'Linkedin URL', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'linkedinurl',
// 		'type' => 'text_url',
// 	) );
//
// 	$cmb_user->add_field( array(
// 		'name' => __( 'User Field', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'user_text_field',
// 		'type' => 'text',
// 	) );
//
// }
//
// function elami_register_taxonomy_metabox() {
// 	$prefix = 'elami_term_';

// 	/**
// 	 * Metabox to add fields to categories and tags
// 	 */
// 	$cmb_term = new_cmb2_box( array(
// 		'id'               => $prefix . 'edit',
// 		'title'            => __( 'Category Metabox', 'elami' ), // Doesn't output for term boxes
// 		'object_types'     => array( 'term' ), // Tells CMB2 to use term_meta vs post_meta
// 		'taxonomies'       => array( 'category', 'post_tag' ), // Tells CMB2 which taxonomies should have these fields
// 		// 'new_term_section' => true, // Will display in the "Add New Category" section
// 	) );

// 	$cmb_term->add_field( array(
// 		'name'     => __( 'Extra Info', 'elami' ),
// 		'desc'     => __( 'field description (optional)', 'elami' ),
// 		'id'       => $prefix . 'extra_info',
// 		'type'     => 'title',
// 		'on_front' => false,
// 	) );

// 	$cmb_term->add_field( array(
// 		'name' => __( 'Term Image', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'avatar',
// 		'type' => 'file',
// 	) );

// 	$cmb_term->add_field( array(
// 		'name' => __( 'Arbitrary Term Field', 'elami' ),
// 		'desc' => __( 'field description (optional)', 'elami' ),
// 		'id'   => $prefix . 'term_text_field',
// 		'type' => 'text',
// 	) );

// }

// add_action( 'cmb2_admin_init', 'elami_register_taxonomy_metabox' );


// /**
//  * Conditionally displays a message if the $post_id is 2
//  *
//  * @param  array             $field_args Array of field parameters
//  * @param  CMB2_Field object $field      Field object
//  */
// function elami_before_row_if_2( $field_args, $field ) {
// 	if ( 2 == $field->object_id ) {
// 		echo '<p>Testing <b>"before_row"</b> parameter (on $post_id 2)</p>';
// 	} else {
// 		echo '<p>Testing <b>"before_row"</b> parameter (<b>NOT</b> on $post_id 2)</p>';
// 	}
// }
