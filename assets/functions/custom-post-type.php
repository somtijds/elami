<?php
/* joints Client Type Example
This page walks you through creating
a Client type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/

function elami_add_taxonomies() {
	// now let's add custom categories (these act like categories)
    register_taxonomy( 'series',
    	array('book'), /* if you change the name of register_post_type( 'client', then you have to change this */
    	array('hierarchical' => false,     /* if this is true, it acts like categories */
    		'labels' => array(
    			'name' => __( 'Series', 'elami' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Series', 'elami' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Series', 'elami' ), /* search title for taxomony */
    			'all_items' => __( 'All Series', 'elami' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Series', 'elami' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Series:', 'elami' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Series', 'elami' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Series', 'elami' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Series', 'elami' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Series Name', 'elami' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'series' ),
    	)
    );

	register_taxonomy( 'format',
		array('book'), /* if you change the name of register_post_type( 'client', then you have to change this */
		array('hierarchical' => false,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Format', 'elami' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Format', 'elami' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Format', 'elami' ), /* search title for taxomony */
				'all_items' => __( 'All Format', 'elami' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Format', 'elami' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Format:', 'elami' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Format', 'elami' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Format', 'elami' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Format', 'elami' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Format Name', 'elami' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'format' ),
		)
	);

		register_taxonomy( 'publisher',
		array('book'), /* if you change the name of register_post_type( 'client', then you have to change this */
		array('hierarchical' => false,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Publisher', 'elami' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Publisher', 'elami' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Publisher', 'elami' ), /* search title for taxomony */
				'all_items' => __( 'All Publisher', 'elami' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Publisher', 'elami' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Publisher:', 'elami' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Publisher', 'elami' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Publisher', 'elami' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Publisher', 'elami' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Publisher Name', 'elami' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'publisher' ),
		)
	);

	// now let's add custom tags (these act like categories)
    register_taxonomy( 'artist_type',
    	array('artist'),
    	array('hierarchical' => false,    /* if this is false, it acts like tags */
    		'labels' => array(
    			'name' => __( 'Artist Types', 'elami' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Artist Type', 'elami' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Artist Types', 'elami' ), /* search title for taxomony */
    			'all_items' => __( 'All Artist Types', 'elami' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Artist Type', 'elami' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Artist Type:', 'elami' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Artist Type', 'elami' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Artist Type', 'elami' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Artist Type', 'elami' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Artist Type Name', 'elami' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    	)
    );

    register_taxonomy( 'available_rights',
        array('book'), /* if you change the name of register_post_type( 'client', then you have to change this */
        array('hierarchical' => false,     /* if this is true, it acts like categories */
            'labels' => array(
                'name' => __( 'Available Rights', 'elami' ), /* name of the custom taxonomy */
                'singular_name' => __( 'Available Rights', 'elami' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Available Rights', 'elami' ), /* search title for taxomony */
                'all_items' => __( 'All Available Rights', 'elami' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent Available Rights', 'elami' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent Available Rights:', 'elami' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Available Rights', 'elami' ), /* edit custom taxonomy title */
                'update_item' => __( 'Update Available Rights', 'elami' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Available Rights', 'elami' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Available Rights Name', 'elami' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'available-rights' ),
        )
    );
}


// let's create the function for the custom type
function elami_add_cpts() {
	// creating (registering) the custom type
	register_post_type( 'artist', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Artists', 'elami'), /* This is the Title of the Group */
			'singular_name' => __('Artist', 'elami'), /* This is the individual type */
			'all_items' => __('All Artists', 'elami'), /* the all items menu item */
			'add_new' => __('Add New', 'elami'), /* The add new menu item */
			'add_new_item' => __('Add New Custom Type', 'elami'), /* Add New Display Title */
			'edit' => __( 'Edit', 'elami' ), /* Edit Dialog */
			'edit_item' => __('Edit Artist', 'elami'), /* Edit Display Title */
			'new_item' => __('New Artist', 'elami'), /* New Display Title */
			'view_item' => __('View Artist', 'elami'), /* View Display Title */
			'search_items' => __('Search Artist', 'elami'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'elami'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'elami'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the post type that will be used for artist data', 'elami' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-book', /* the icon for the Artist type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'artists', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'artists', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */

	// creating (registering) the custom type
	register_post_type( 'book', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Books', 'elami'), /* This is the title of the Group */
			'singular_name' => __('Book', 'elami'), /* This is the individual type */
			'all_items' => __('All Books', 'elami'), /* the all items menu item */
			'add_new' => __('Add New', 'elami'), /* The add new menu item */
			'add_new_item' => __('Add New Book', 'elami'), /* Add New Display Title */
			'edit' => __( 'Edit', 'elami' ), /* Edit Dialog */
			'edit_item' => __('Edit Book', 'elami'), /* Edit Display Title */
			'new_item' => __('New Book', 'elami'), /* New Display Title */
			'view_item' => __('View Book', 'elami'), /* View Display Title */
			'search_items' => __('Search Book', 'elami'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'elami'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'elami'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the post type that will be used for book / series info', 'elami' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-book', /* the icon for the Artist type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'books', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'books', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post tags to the Title type */
	register_taxonomy_for_object_type('post_tag', 'book');

	/* this adds your post tags to the Title type */
	register_taxonomy_for_object_type('series', 'book');

	/* this adds your post tags to the Title type */
	register_taxonomy_for_object_type('format', 'book');

	/* this adds your post tags to the Artist type */
	register_taxonomy_for_object_type('artist_type', 'artist');

}

// adding the function to the Wordpress init
add_action( 'init', 'elami_add_taxonomies' );
add_action( 'init', 'elami_add_cpts' );

add_action( 'pre_get_posts', 'elami_edit_artist_query_vars' );
function elami_edit_artist_query_vars( $query_obj ) {


    if ( isset( $query_obj->query['post_type'] ) && 'artist' == $query_obj->query['post_type'] ) {

        // Escape if the meta_query is already set.
        if ( ! empty( $query_obj->query['meta_query'] ) && 'elami_artist_management' === $query_obj->query['meta_query'][0]['key'] ) {
			return $query_obj;
		}

        // Add management meta query plus sorting options.
		$query_obj->set( 'meta_query', array(
			'relation' => 'OR',
			array(
				'key' => 'elami_artist_management',
				'value' => array( 'on', null )
			),
			array(
				'key' => 'elami_artist_management',
				'compare' => 'NOT EXISTS',
			),
		) );
		$query_obj->set( 'orderby', array(
		    'meta_value_num' => 'ASC',
            'date' => 'DESC',
            )
        );
		$query_obj->set( 'order', 'ASC' );
	}
	return $query_obj;
}



add_filter( 'query_vars', 'elami_add_artist_query_vars_filter' );
// Add artist query variable for filtering.
function elami_add_artist_query_vars_filter( $vars ) {
  $vars[] = "artist_id";
  return $vars;
}

add_action( 'pre_get_posts', 'elami_edit_book_query_vars' );
// Extract non-signed artists, add filter for artists
function elami_edit_book_query_vars( $query ) {
	if ( is_admin() ) {
		return $query;
	}
 	$artist_id = get_query_var('artist_id');
	if ( empty( $artist_id )
		&& $query->is_main_query()
		&& $query->is_archive()
		&& ( 'artist' === $query->query['post_type'] ) ) {
			// Remove any unsigned artists from the loop.
			$query->set( 'tax_query', array(
				array(
					'taxonomy' => 'artist_type',
					'terms' => array( 'unsigned' ),
					'field' => 'slug',
					'operator' => 'NOT IN',
					)
				)
			);
	}
	if ( ! empty( $artist_id )
		&& $query->is_archive()
		&& $query->is_main_query()
		&& 'book' === $query->query['post_type'] ) {
			// Verify that the post exists.
			if ( ! elami_artist_exists( $artist_id ) ) {
				return $query;
			}
			// Remove any unsigned artists from the loop.
			$query->set( 'meta_query', array(
				'relation' => 'OR',
				array(
					'key' => 'elami_book_attached_authors',
					'value' => sprintf(':"%s"', $artist_id ),
					'compare' => 'LIKE',
				),
				array(
					'key' => 'elami_book_attached_illustrators',
					'value' => sprintf(':"%s"', $artist_id ),
					'compare' => 'LIKE',
				),
			) );
	}
	return $query;
}

add_filter( 'term_links-artist_type', 'elami_make_artist_types_singular' );
function elami_make_artist_types_singular( $links ) {
	$singular_links = array();
	foreach( $links as $link ) {
		// Hack to make artist types singular, simply remove the 's'
		$link = str_replace( 's</a>', '</a>', $link );
		$singular_links[] = $link;
	}
	return $singular_links;
}