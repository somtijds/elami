<?php	

/**
 * Sanitize text input
 */
function elami_sanitize_text( $input ) {
	return wp_kses_post( force_balance_tags( $input ) );
}

/**
 * Sanitize page drop down
 */
function elami_sanitize_integer( $input ) {
	
	if( is_numeric( $input ) ) {
		return intval( $input );
	}
}

if( class_exists( 'WP_Customize_Control' ) ) {
	class WP_Customize_Various_Links_Control extends WP_Customize_Control {
		public $type = 'various-dropdown';
 
		public function render_content() {

			$pages = new WP_Query( array(
				'post_type'      => array( 'page' ),
				'post_status'    => 'publish',
				'orderby'        => 'date',
				'order'          => 'DESC',
				'posts_per_page' => -1,
			));
			$books = new WP_Query( array(
				'post_type'      => array( 'book' ),
				'post_status'    => 'publish',
				'orderby'        => 'date',
				'order'          => 'DESC',
				'posts_per_page' => 5,
			));
			$artists = new WP_Query( array(
				'post_type'      => array( 'artist' ),
				'post_status'    => 'publish',
				'orderby'        => 'date',
				'order'          => 'DESC',
				'posts_per_page' => 5,
				'tax_query'	 => array(
					array(
						'taxonomy' => 'artist_type',
						'terms' => array( 'unsigned' ),
						'field' => 'slug',
						'operator' => 'NOT IN',
						)
				),
			));
			?>
				<label>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
					<select <?php $this->link(); ?>>
						<optgroup label="pages">
						<?php 
						while( $pages->have_posts() ) {

							$pages->the_post();
							echo "<option " . selected( $this->value(), get_the_ID() ) . " value='" . get_the_ID() . "'>" . the_title( '', '', false ) . "</option>";
						}

						wp_reset_query();
						?>
						</optgroup>
						<optgroup label="books">
						<?php 
						while( $books->have_posts() ) {
							$books->the_post();
							echo "<option " . selected( $this->value(), get_the_ID() ) . " value='" . get_the_ID() . "'>" . the_title( '', '', false ) . "</option>";
						}

						wp_reset_query();
						?>

						</optgroup>
						<optgroup label="artists">
						<?php 
						while( $artists->have_posts() ) {
							$artists->the_post();

							echo "<option " . selected( $this->value(), get_the_ID() ) . " value='" . get_the_ID() . "'>" . the_title( '', '', false ) . "</option>";
						}
						wp_reset_query();
						?>
						</optgroup>
					</select>
				</label>
			<?php
		}
	}
}

/**
 * @param WP_Customize_Manager $wp_customize
 */
function elami_customizer_register( $wp_customize ) {

	/**
	 * Homepage Feature Text
	 */
	$wp_customize->add_setting( 'elami_home_feature_image', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'elami_sanitize_text',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'elami_home_feature_image', array(
		'label'    => esc_html__( 'Featured Item Image', 'elami' ),
		'section'  => 'static_front_page',
		'settings' => 'elami_home_feature_image',
		'priority' => 10,
	) ) );

	/**
	 * Homepage Feature Text
	 */
	$wp_customize->add_setting( 'elami_home_feature_text', array(
		'transport'         => 'postMessage',
		'sanitize_callback' => 'elami_sanitize_text',
	) );

	$wp_customize->add_control( 'elami_home_feature_text', array(
		'label'    => esc_html__( 'Featured Item Text', 'elami' ),
		'section'  => 'static_front_page',
		'settings' => 'elami_home_feature_text',
		'priority' => 10,
	) );

	/**
	 * Homepage Feature Link
	 */
	$wp_customize->add_setting( 'elami_home_feature_link', array(
		'default'           => '',
		'sanitize_callback' => 'elami_sanitize_integer',
	) );

	$wp_customize->add_control( new WP_Customize_Various_Links_Control( $wp_customize, 'elami_home_feature_link', array(
		'type'     => 'various-dropdown',
		'label'    => esc_html__( 'Featured Item Link', 'elami' ),
		'settings' => 'elami_home_feature_link',
		'section'  => 'static_front_page',
		'priority' => 10,
	) ) );

		/**
	 * Homepage Button One Link
	 */
	$wp_customize->add_setting( 'elami_home_button_one_link', array(
		'default'           => '',
		'sanitize_callback' => 'elami_sanitize_integer',
	) );

	$wp_customize->add_control( new WP_Customize_Various_Links_Control( $wp_customize, 'elami_home_button_one_link', array(
		'type'     => 'various-dropdown',
		'label'    => esc_html__( 'Button One Link', 'elami' ),
		'settings' => 'elami_home_button_one_link',
		'section'  => 'static_front_page',
		'priority' => 10,
	) ) );

	/**
	 * Homepage Button One Text
	 */
	$wp_customize->add_setting( 'elami_home_button_one_text', array(
		'default'           => esc_html__( "I'm an artist", 'elami' ),
		'sanitize_callback' => 'elami_sanitize_text',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'elami_home_button_one_text', array(
		'label'           => esc_html__( 'Button One Text', 'elami' ),
		'section'         => 'static_front_page',
		'settings'        => 'elami_home_button_one_text',
		'type'            => 'text',
		'priority'        => 10,
	) );

	/**
	 * Homepage Button Two Link
	 */
	$wp_customize->add_setting( 'elami_home_button_two_link', array(
		'default'           => '',
		'sanitize_callback' => 'elami_sanitize_integer',
	) );

	$wp_customize->add_control( new WP_Customize_Various_Links_Control( $wp_customize, 'elami_home_button_two_link', array(
		'type'     => 'various-dropdown',
		'label'    => esc_html__( 'Button Two Link', 'elami' ),
		'settings' => 'elami_home_button_two_link',
		'section'  => 'static_front_page',
		'priority' => 10,
	) ) );

	/**
	 * Homepage Button Two Text
	 */
	$wp_customize->add_setting( 'elami_home_button_two_text', array(
		'default'           => esc_html__( "I'm a publisher", 'elami' ),
		'sanitize_callback' => 'elami_sanitize_text',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'elami_home_button_two_text', array(
		'label'           => esc_html__( 'Button Two Text', 'elami' ),
		'section'         => 'static_front_page',
		'settings'        => 'elami_home_button_two_text',
		'type'            => 'text',
		'priority'        => 10,
	) );

}
add_action( 'customize_register', 'elami_customizer_register' );

/**
 * Output custom options from Customizer
 */
function elami_customizer_css() {
	if ( ! is_front_page() ) {
		return;
	}
	?>
	<style type="text/css">
	<?php
		$home_id = get_option( 'page_on_front' );
		$home_feature_image = get_theme_mod( 'elami_home_feature_image');
		if ( empty( $home_id ) 
			|| empty( $home_feature_image )
			|| 'page' !== get_option('show_on_front') ) {
			return;
		}
	?>
		.home {
			background-image: url('<?php echo $home_feature_image ?>');
			background-size: cover;
			background-position: center center;
		 }

	</style>
	<?php
}
add_action( 'wp_head', 'elami_customizer_css' );