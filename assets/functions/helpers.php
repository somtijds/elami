<?php

/**
 * Return array of translated language strings
 *
 * @return void
 * @author Willem Prins | Somtijds
 * @link via: https://github.com/umpirsky/language-list/blob/master/data/en_GB/language.php
 **/
function elami_language_list() {
	return array(
	  'ab' => esc_html__( 'Abkhazian','elami' ),
	  'ace' => esc_html__( 'Achinese','elami' ),
	  'ach' => esc_html__( 'Acoli','elami' ),
	  'ada' => esc_html__( 'Adangme','elami' ),
	  'ady' => esc_html__( 'Adyghe','elami' ),
	  'aa' => esc_html__( 'Afar','elami' ),
	  'afh' => esc_html__( 'Afrihili','elami' ),
	  'af' => esc_html__( 'Afrikaans','elami' ),
	  'agq' => esc_html__( 'Aghem','elami' ),
	  'ain' => esc_html__( 'Ainu','elami' ),
	  'ak' => esc_html__( 'Akan','elami' ),
	  'akk' => esc_html__( 'Akkadian','elami' ),
	  'bss' => esc_html__( 'Akoose','elami' ),
	  'akz' => esc_html__( 'Alabama','elami' ),
	  'sq' => esc_html__( 'Albanian','elami' ),
	  'ale' => esc_html__( 'Aleut','elami' ),
	  'arq' => esc_html__( 'Algerian Arabic','elami' ),
	  'en_US' => esc_html__( 'American English','elami' ),
	  'ase' => esc_html__( 'American Sign Language','elami' ),
	  'am' => esc_html__( 'Amharic','elami' ),
	  'egy' => esc_html__( 'Ancient Egyptian','elami' ),
	  'grc' => esc_html__( 'Ancient Greek','elami' ),
	  'anp' => esc_html__( 'Angika','elami' ),
	  'njo' => esc_html__( 'Ao Naga','elami' ),
	  'ar' => esc_html__( 'Arabic','elami' ),
	  'an' => esc_html__( 'Aragonese','elami' ),
	  'arc' => esc_html__( 'Aramaic','elami' ),
	  'aro' => esc_html__( 'Araona','elami' ),
	  'arp' => esc_html__( 'Arapaho','elami' ),
	  'arw' => esc_html__( 'Arawak','elami' ),
	  'hy' => esc_html__( 'Armenian','elami' ),
	  'rup' => esc_html__( 'Aromanian','elami' ),
	  'frp' => esc_html__( 'Arpitan','elami' ),
	  'as' => esc_html__( 'Assamese','elami' ),
	  'ast' => esc_html__( 'Asturian','elami' ),
	  'asa' => esc_html__( 'Asu','elami' ),
	  'cch' => esc_html__( 'Atsam','elami' ),
	  'en_AU' => esc_html__( 'Australian English','elami' ),
	  'de_AT' => esc_html__( 'Austrian German','elami' ),
	  'av' => esc_html__( 'Avaric','elami' ),
	  'ae' => esc_html__( 'Avestan','elami' ),
	  'awa' => esc_html__( 'Awadhi','elami' ),
	  'ay' => esc_html__( 'Aymara','elami' ),
	  'az' => esc_html__( 'Azerbaijani','elami' ),
	  'bfq' => esc_html__( 'Badaga','elami' ),
	  'ksf' => esc_html__( 'Bafia','elami' ),
	  'bfd' => esc_html__( 'Bafut','elami' ),
	  'bqi' => esc_html__( 'Bakhtiari','elami' ),
	  'ban' => esc_html__( 'Balinese','elami' ),
	  'bal' => esc_html__( 'Baluchi','elami' ),
	  'bm' => esc_html__( 'Bambara','elami' ),
	  'bax' => esc_html__( 'Bamun','elami' ),
	  'bjn' => esc_html__( 'Banjar','elami' ),
	  'bas' => esc_html__( 'Basaa','elami' ),
	  'ba' => esc_html__( 'Bashkir','elami' ),
	  'eu' => esc_html__( 'Basque','elami' ),
	  'bbc' => esc_html__( 'Batak Toba','elami' ),
	  'bar' => esc_html__( 'Bavarian','elami' ),
	  'bej' => esc_html__( 'Beja','elami' ),
	  'be' => esc_html__( 'Belarusian','elami' ),
	  'bem' => esc_html__( 'Bemba','elami' ),
	  'bez' => esc_html__( 'Bena','elami' ),
	  'bn' => esc_html__( 'Bengali','elami' ),
	  'bew' => esc_html__( 'Betawi','elami' ),
	  'bho' => esc_html__( 'Bhojpuri','elami' ),
	  'bik' => esc_html__( 'Bikol','elami' ),
	  'bin' => esc_html__( 'Bini','elami' ),
	  'bpy' => esc_html__( 'Bishnupriya','elami' ),
	  'bi' => esc_html__( 'Bislama','elami' ),
	  'byn' => esc_html__( 'Blin','elami' ),
	  'zbl' => esc_html__( 'Blissymbols','elami' ),
	  'brx' => esc_html__( 'Bodo','elami' ),
	  'bs' => esc_html__( 'Bosnian','elami' ),
	  'brh' => esc_html__( 'Brahui','elami' ),
	  'bra' => esc_html__( 'Braj','elami' ),
	  'pt_BR' => esc_html__( 'Brazilian Portuguese','elami' ),
	  'br' => esc_html__( 'Breton','elami' ),
	  'en_GB' => esc_html__( 'British English','elami' ),
	  'bug' => esc_html__( 'Buginese','elami' ),
	  'bg' => esc_html__( 'Bulgarian','elami' ),
	  'bum' => esc_html__( 'Bulu','elami' ),
	  'bua' => esc_html__( 'Buriat','elami' ),
	  'my' => esc_html__( 'Burmese','elami' ),
	  'cad' => esc_html__( 'Caddo','elami' ),
	  'frc' => esc_html__( 'Cajun French','elami' ),
	  'en_CA' => esc_html__( 'Canadian English','elami' ),
	  'fr_CA' => esc_html__( 'Canadian French','elami' ),
	  'yue' => esc_html__( 'Cantonese','elami' ),
	  'cps' => esc_html__( 'Capiznon','elami' ),
	  'car' => esc_html__( 'Carib','elami' ),
	  'ca' => esc_html__( 'Catalan','elami' ),
	  'cay' => esc_html__( 'Cayuga','elami' ),
	  'ceb' => esc_html__( 'Cebuano','elami' ),
	  'tzm' => esc_html__( 'Central Atlas Tamazight','elami' ),
	  'dtp' => esc_html__( 'Central Dusun','elami' ),
	  'esu' => esc_html__( 'Central Yupik','elami' ),
	  'shu' => esc_html__( 'Chadian Arabic','elami' ),
	  'chg' => esc_html__( 'Chagatai','elami' ),
	  'ch' => esc_html__( 'Chamorro','elami' ),
	  'ce' => esc_html__( 'Chechen','elami' ),
	  'chr' => esc_html__( 'Cherokee','elami' ),
	  'chy' => esc_html__( 'Cheyenne','elami' ),
	  'chb' => esc_html__( 'Chibcha','elami' ),
	  'cgg' => esc_html__( 'Chiga','elami' ),
	  'qug' => esc_html__( 'Chimborazo Highland Quichua','elami' ),
	  'zh' => esc_html__( 'Chinese','elami' ),
	  'chn' => esc_html__( 'Chinook Jargon','elami' ),
	  'chp' => esc_html__( 'Chipewyan','elami' ),
	  'cho' => esc_html__( 'Choctaw','elami' ),
	  'cu' => esc_html__( 'Church Slavic','elami' ),
	  'chk' => esc_html__( 'Chuukese','elami' ),
	  'cv' => esc_html__( 'Chuvash','elami' ),
	  'nwc' => esc_html__( 'Classical Newari','elami' ),
	  'syc' => esc_html__( 'Classical Syriac','elami' ),
	  'ksh' => esc_html__( 'Colognian','elami' ),
	  'swb' => esc_html__( 'Comorian','elami' ),
	  'swc' => esc_html__( 'Congo Swahili','elami' ),
	  'cop' => esc_html__( 'Coptic','elami' ),
	  'kw' => esc_html__( 'Cornish','elami' ),
	  'co' => esc_html__( 'Corsican','elami' ),
	  'cr' => esc_html__( 'Cree','elami' ),
	  'mus' => esc_html__( 'Creek','elami' ),
	  'crh' => esc_html__( 'Crimean Turkish','elami' ),
	  'hr' => esc_html__( 'Croatian','elami' ),
	  'cs' => esc_html__( 'Czech','elami' ),
	  'dak' => esc_html__( 'Dakota','elami' ),
	  'da' => esc_html__( 'Danish','elami' ),
	  'dar' => esc_html__( 'Dargwa','elami' ),
	  'dzg' => esc_html__( 'Dazaga','elami' ),
	  'del' => esc_html__( 'Delaware','elami' ),
	  'din' => esc_html__( 'Dinka','elami' ),
	  'dv' => esc_html__( 'Divehi','elami' ),
	  'doi' => esc_html__( 'Dogri','elami' ),
	  'dgr' => esc_html__( 'Dogrib','elami' ),
	  'dua' => esc_html__( 'Duala','elami' ),
	  'nl' => esc_html__( 'Dutch','elami' ),
	  'dyu' => esc_html__( 'Dyula','elami' ),
	  'dz' => esc_html__( 'Dzongkha','elami' ),
	  'frs' => esc_html__( 'Eastern Frisian','elami' ),
	  'efi' => esc_html__( 'Efik','elami' ),
	  'arz' => esc_html__( 'Egyptian Arabic','elami' ),
	  'eka' => esc_html__( 'Ekajuk','elami' ),
	  'elx' => esc_html__( 'Elamite','elami' ),
	  'ebu' => esc_html__( 'Embu','elami' ),
	  'egl' => esc_html__( 'Emilian','elami' ),
	  'en' => esc_html__( 'English','elami' ),
	  'myv' => esc_html__( 'Erzya','elami' ),
	  'eo' => esc_html__( 'Esperanto','elami' ),
	  'et' => esc_html__( 'Estonian','elami' ),
	  'pt_PT' => esc_html__( 'European Portuguese','elami' ),
	  'es_ES' => esc_html__( 'European Spanish','elami' ),
	  'ee' => esc_html__( 'Ewe','elami' ),
	  'ewo' => esc_html__( 'Ewondo','elami' ),
	  'ext' => esc_html__( 'Extremaduran','elami' ),
	  'fan' => esc_html__( 'Fang','elami' ),
	  'fat' => esc_html__( 'Fanti','elami' ),
	  'fo' => esc_html__( 'Faroese','elami' ),
	  'hif' => esc_html__( 'Fiji Hindi','elami' ),
	  'fj' => esc_html__( 'Fijian','elami' ),
	  'fil' => esc_html__( 'Filipino','elami' ),
	  'fi' => esc_html__( 'Finnish','elami' ),
	  'nl_BE' => esc_html__( 'Flemish','elami' ),
	  'fon' => esc_html__( 'Fon','elami' ),
	  'gur' => esc_html__( 'Frafra','elami' ),
	  'fr' => esc_html__( 'French','elami' ),
	  'fry_NL' => esc_html__( 'Frisian','elami' ),
	  'fur' => esc_html__( 'Friulian','elami' ),
	  'ff' => esc_html__( 'Fulah','elami' ),
	  'gaa' => esc_html__( 'Ga','elami' ),
	  'gag' => esc_html__( 'Gagauz','elami' ),
	  'gl' => esc_html__( 'Galician','elami' ),
	  'gan' => esc_html__( 'Gan Chinese','elami' ),
	  'lg' => esc_html__( 'Ganda','elami' ),
	  'gay' => esc_html__( 'Gayo','elami' ),
	  'gba' => esc_html__( 'Gbaya','elami' ),
	  'gez' => esc_html__( 'Geez','elami' ),
	  'ka' => esc_html__( 'Georgian','elami' ),
	  'de' => esc_html__( 'German','elami' ),
	  'aln' => esc_html__( 'Gheg Albanian','elami' ),
	  'bbj' => esc_html__( 'Ghomala','elami' ),
	  'glk' => esc_html__( 'Gilaki','elami' ),
	  'gil' => esc_html__( 'Gilbertese','elami' ),
	  'gom' => esc_html__( 'Goan Konkani','elami' ),
	  'gon' => esc_html__( 'Gondi','elami' ),
	  'gor' => esc_html__( 'Gorontalo','elami' ),
	  'got' => esc_html__( 'Gothic','elami' ),
	  'grb' => esc_html__( 'Grebo','elami' ),
	  'el' => esc_html__( 'Greek','elami' ),
	  'gn' => esc_html__( 'Guarani','elami' ),
	  'gu' => esc_html__( 'Gujarati','elami' ),
	  'guz' => esc_html__( 'Gusii','elami' ),
	  'gwi' => esc_html__( 'Gwichʼin','elami' ),
	  'hai' => esc_html__( 'Haida','elami' ),
	  'ht' => esc_html__( 'Haitian','elami' ),
	  'hak' => esc_html__( 'Hakka Chinese','elami' ),
	  'ha' => esc_html__( 'Hausa','elami' ),
	  'haw' => esc_html__( 'Hawaiian','elami' ),
	  'he' => esc_html__( 'Hebrew','elami' ),
	  'hz' => esc_html__( 'Herero','elami' ),
	  'hil' => esc_html__( 'Hiligaynon','elami' ),
	  'hi' => esc_html__( 'Hindi','elami' ),
	  'ho' => esc_html__( 'Hiri Motu','elami' ),
	  'hit' => esc_html__( 'Hittite','elami' ),
	  'hmn' => esc_html__( 'Hmong','elami' ),
	  'hu' => esc_html__( 'Hungarian','elami' ),
	  'hup' => esc_html__( 'Hupa','elami' ),
	  'iba' => esc_html__( 'Iban','elami' ),
	  'ibb' => esc_html__( 'Ibibio','elami' ),
	  'is' => esc_html__( 'Icelandic','elami' ),
	  'io' => esc_html__( 'Ido','elami' ),
	  'ig' => esc_html__( 'Igbo','elami' ),
	  'ilo' => esc_html__( 'Iloko','elami' ),
	  'smn' => esc_html__( 'Inari Sami','elami' ),
	  'id' => esc_html__( 'Indonesian','elami' ),
	  'izh' => esc_html__( 'Ingrian','elami' ),
	  'inh' => esc_html__( 'Ingush','elami' ),
	  'ia' => esc_html__( 'Interlingua','elami' ),
	  'ie' => esc_html__( 'Interlingue','elami' ),
	  'iu' => esc_html__( 'Inuktitut','elami' ),
	  'ik' => esc_html__( 'Inupiaq','elami' ),
	  'ga' => esc_html__( 'Irish','elami' ),
	  'it' => esc_html__( 'Italian','elami' ),
	  'jam' => esc_html__( 'Jamaican Creole English','elami' ),
	  'ja' => esc_html__( 'Japanese','elami' ),
	  'jv' => esc_html__( 'Javanese','elami' ),
	  'kaj' => esc_html__( 'Jju','elami' ),
	  'dyo' => esc_html__( 'Jola-Fonyi','elami' ),
	  'jrb' => esc_html__( 'Judeo-Arabic','elami' ),
	  'jpr' => esc_html__( 'Judeo-Persian','elami' ),
	  'jut' => esc_html__( 'Jutish','elami' ),
	  'kbd' => esc_html__( 'Kabardian','elami' ),
	  'kea' => esc_html__( 'Kabuverdianu','elami' ),
	  'kab' => esc_html__( 'Kabyle','elami' ),
	  'kac' => esc_html__( 'Kachin','elami' ),
	  'kgp' => esc_html__( 'Kaingang','elami' ),
	  'kkj' => esc_html__( 'Kako','elami' ),
	  'kl' => esc_html__( 'Kalaallisut','elami' ),
	  'kln' => esc_html__( 'Kalenjin','elami' ),
	  'xal' => esc_html__( 'Kalmyk','elami' ),
	  'kam' => esc_html__( 'Kamba','elami' ),
	  'kbl' => esc_html__( 'Kanembu','elami' ),
	  'kn' => esc_html__( 'Kannada','elami' ),
	  'kr' => esc_html__( 'Kanuri','elami' ),
	  'kaa' => esc_html__( 'Kara-Kalpak','elami' ),
	  'krc' => esc_html__( 'Karachay-Balkar','elami' ),
	  'krl' => esc_html__( 'Karelian','elami' ),
	  'ks' => esc_html__( 'Kashmiri','elami' ),
	  'csb' => esc_html__( 'Kashubian','elami' ),
	  'kaw' => esc_html__( 'Kawi','elami' ),
	  'kk' => esc_html__( 'Kazakh','elami' ),
	  'ken' => esc_html__( 'Kenyang','elami' ),
	  'kha' => esc_html__( 'Khasi','elami' ),
	  'km' => esc_html__( 'Khmer','elami' ),
	  'kho' => esc_html__( 'Khotanese','elami' ),
	  'khw' => esc_html__( 'Khowar','elami' ),
	  'ki' => esc_html__( 'Kikuyu','elami' ),
	  'kmb' => esc_html__( 'Kimbundu','elami' ),
	  'krj' => esc_html__( 'Kinaray-a','elami' ),
	  'rw' => esc_html__( 'Kinyarwanda','elami' ),
	  'kiu' => esc_html__( 'Kirmanjki','elami' ),
	  'tlh' => esc_html__( 'Klingon','elami' ),
	  'bkm' => esc_html__( 'Kom','elami' ),
	  'kv' => esc_html__( 'Komi','elami' ),
	  'koi' => esc_html__( 'Komi-Permyak','elami' ),
	  'kg' => esc_html__( 'Kongo','elami' ),
	  'kok' => esc_html__( 'Konkani','elami' ),
	  'ko' => esc_html__( 'Korean','elami' ),
	  'kfo' => esc_html__( 'Koro','elami' ),
	  'kos' => esc_html__( 'Kosraean','elami' ),
	  'avk' => esc_html__( 'Kotava','elami' ),
	  'khq' => esc_html__( 'Koyra Chiini','elami' ),
	  'ses' => esc_html__( 'Koyraboro Senni','elami' ),
	  'kpe' => esc_html__( 'Kpelle','elami' ),
	  'kri' => esc_html__( 'Krio','elami' ),
	  'kj' => esc_html__( 'Kuanyama','elami' ),
	  'kum' => esc_html__( 'Kumyk','elami' ),
	  'ku' => esc_html__( 'Kurdish','elami' ),
	  'kru' => esc_html__( 'Kurukh','elami' ),
	  'kut' => esc_html__( 'Kutenai','elami' ),
	  'nmg' => esc_html__( 'Kwasio','elami' ),
	  'ky' => esc_html__( 'Kyrgyz','elami' ),
	  'quc' => esc_html__( 'Kʼicheʼ','elami' ),
	  'lad' => esc_html__( 'Ladino','elami' ),
	  'lah' => esc_html__( 'Lahnda','elami' ),
	  'lkt' => esc_html__( 'Lakota','elami' ),
	  'lam' => esc_html__( 'Lamba','elami' ),
	  'lag' => esc_html__( 'Langi','elami' ),
	  'lo' => esc_html__( 'Lao','elami' ),
	  'ltg' => esc_html__( 'Latgalian','elami' ),
	  'la' => esc_html__( 'Latin','elami' ),
	  'es_419' => esc_html__( 'Latin American Spanish','elami' ),
	  'lv' => esc_html__( 'Latvian','elami' ),
	  'lzz' => esc_html__( 'Laz','elami' ),
	  'lez' => esc_html__( 'Lezghian','elami' ),
	  'lij' => esc_html__( 'Ligurian','elami' ),
	  'li' => esc_html__( 'Limburgish','elami' ),
	  'ln' => esc_html__( 'Lingala','elami' ),
	  'lfn' => esc_html__( 'Lingua Franca Nova','elami' ),
	  'lzh' => esc_html__( 'Literary Chinese','elami' ),
	  'lt' => esc_html__( 'Lithuanian','elami' ),
	  'liv' => esc_html__( 'Livonian','elami' ),
	  'jbo' => esc_html__( 'Lojban','elami' ),
	  'lmo' => esc_html__( 'Lombard','elami' ),
	  'nds' => esc_html__( 'Low German','elami' ),
	  'sli' => esc_html__( 'Lower Silesian','elami' ),
	  'dsb' => esc_html__( 'Lower Sorbian','elami' ),
	  'loz' => esc_html__( 'Lozi','elami' ),
	  'lu' => esc_html__( 'Luba-Katanga','elami' ),
	  'lua' => esc_html__( 'Luba-Lulua','elami' ),
	  'lui' => esc_html__( 'Luiseno','elami' ),
	  'smj' => esc_html__( 'Lule Sami','elami' ),
	  'lun' => esc_html__( 'Lunda','elami' ),
	  'luo' => esc_html__( 'Luo','elami' ),
	  'lb' => esc_html__( 'Luxembourgish','elami' ),
	  'luy' => esc_html__( 'Luyia','elami' ),
	  'mde' => esc_html__( 'Maba','elami' ),
	  'mk' => esc_html__( 'Macedonian','elami' ),
	  'jmc' => esc_html__( 'Machame','elami' ),
	  'mad' => esc_html__( 'Madurese','elami' ),
	  'maf' => esc_html__( 'Mafa','elami' ),
	  'mag' => esc_html__( 'Magahi','elami' ),
	  'vmf' => esc_html__( 'Main-Franconian','elami' ),
	  'mai' => esc_html__( 'Maithili','elami' ),
	  'mak' => esc_html__( 'Makasar','elami' ),
	  'mgh' => esc_html__( 'Makhuwa-Meetto','elami' ),
	  'kde' => esc_html__( 'Makonde','elami' ),
	  'mg' => esc_html__( 'Malagasy','elami' ),
	  'ms' => esc_html__( 'Malay','elami' ),
	  'ml' => esc_html__( 'Malayalam','elami' ),
	  'mt' => esc_html__( 'Maltese','elami' ),
	  'mnc' => esc_html__( 'Manchu','elami' ),
	  'mdr' => esc_html__( 'Mandar','elami' ),
	  'man' => esc_html__( 'Mandingo','elami' ),
	  'mni' => esc_html__( 'Manipuri','elami' ),
	  'gv' => esc_html__( 'Manx','elami' ),
	  'mi' => esc_html__( 'Maori','elami' ),
	  'arn' => esc_html__( 'Mapuche','elami' ),
	  'mr' => esc_html__( 'Marathi','elami' ),
	  'chm' => esc_html__( 'Mari','elami' ),
	  'mh' => esc_html__( 'Marshallese','elami' ),
	  'mwr' => esc_html__( 'Marwari','elami' ),
	  'mas' => esc_html__( 'Masai','elami' ),
	  'mzn' => esc_html__( 'Mazanderani','elami' ),
	  'byv' => esc_html__( 'Medumba','elami' ),
	  'men' => esc_html__( 'Mende','elami' ),
	  'mwv' => esc_html__( 'Mentawai','elami' ),
	  'mer' => esc_html__( 'Meru','elami' ),
	  'mgo' => esc_html__( 'Metaʼ','elami' ),
	  'es_MX' => esc_html__( 'Mexican Spanish','elami' ),
	  'mic' => esc_html__( 'Micmac','elami' ),
	  'dum' => esc_html__( 'Middle Dutch','elami' ),
	  'enm' => esc_html__( 'Middle English','elami' ),
	  'frm' => esc_html__( 'Middle French','elami' ),
	  'gmh' => esc_html__( 'Middle High German','elami' ),
	  'mga' => esc_html__( 'Middle Irish','elami' ),
	  'nan' => esc_html__( 'Min Nan Chinese','elami' ),
	  'min' => esc_html__( 'Minangkabau','elami' ),
	  'xmf' => esc_html__( 'Mingrelian','elami' ),
	  'mwl' => esc_html__( 'Mirandese','elami' ),
	  'lus' => esc_html__( 'Mizo','elami' ),
	  'ar_001' => esc_html__( 'Modern Standard Arabic','elami' ),
	  'moh' => esc_html__( 'Mohawk','elami' ),
	  'mdf' => esc_html__( 'Moksha','elami' ),
	  'ro_MD' => esc_html__( 'Moldavian','elami' ),
	  'lol' => esc_html__( 'Mongo','elami' ),
	  'mn' => esc_html__( 'Mongolian','elami' ),
	  'mfe' => esc_html__( 'Morisyen','elami' ),
	  'ary' => esc_html__( 'Moroccan Arabic','elami' ),
	  'mos' => esc_html__( 'Mossi','elami' ),
	  'mul' => esc_html__( 'Multiple Languages','elami' ),
	  'mua' => esc_html__( 'Mundang','elami' ),
	  'ttt' => esc_html__( 'Muslim Tat','elami' ),
	  'mye' => esc_html__( 'Myene','elami' ),
	  'naq' => esc_html__( 'Nama','elami' ),
	  'na' => esc_html__( 'Nauru','elami' ),
	  'nv' => esc_html__( 'Navajo','elami' ),
	  'ng' => esc_html__( 'Ndonga','elami' ),
	  'nap' => esc_html__( 'Neapolitan','elami' ),
	  'ne' => esc_html__( 'Nepali','elami' ),
	  'new' => esc_html__( 'Newari','elami' ),
	  'sba' => esc_html__( 'Ngambay','elami' ),
	  'nnh' => esc_html__( 'Ngiemboon','elami' ),
	  'jgo' => esc_html__( 'Ngomba','elami' ),
	  'yrl' => esc_html__( 'Nheengatu','elami' ),
	  'nia' => esc_html__( 'Nias','elami' ),
	  'niu' => esc_html__( 'Niuean','elami' ),
	  'zxx' => esc_html__( 'No linguistic content','elami' ),
	  'nog' => esc_html__( 'Nogai','elami' ),
	  'nd' => esc_html__( 'North Ndebele','elami' ),
	  'frr' => esc_html__( 'Northern Frisian','elami' ),
	  'se' => esc_html__( 'Northern Sami','elami' ),
	  'nso' => esc_html__( 'Northern Sotho','elami' ),
	  'no' => esc_html__( 'Norwegian','elami' ),
	  'nb' => esc_html__( 'Norwegian Bokmål','elami' ),
	  'nn' => esc_html__( 'Norwegian Nynorsk','elami' ),
	  'nov' => esc_html__( 'Novial','elami' ),
	  'nus' => esc_html__( 'Nuer','elami' ),
	  'nym' => esc_html__( 'Nyamwezi','elami' ),
	  'ny' => esc_html__( 'Nyanja','elami' ),
	  'nyn' => esc_html__( 'Nyankole','elami' ),
	  'tog' => esc_html__( 'Nyasa Tonga','elami' ),
	  'nyo' => esc_html__( 'Nyoro','elami' ),
	  'nzi' => esc_html__( 'Nzima','elami' ),
	  'nqo' => esc_html__( 'NʼKo','elami' ),
	  'oc' => esc_html__( 'Occitan','elami' ),
	  'oj' => esc_html__( 'Ojibwa','elami' ),
	  'ang' => esc_html__( 'Old English','elami' ),
	  'fro' => esc_html__( 'Old French','elami' ),
	  'goh' => esc_html__( 'Old High German','elami' ),
	  'sga' => esc_html__( 'Old Irish','elami' ),
	  'non' => esc_html__( 'Old Norse','elami' ),
	  'peo' => esc_html__( 'Old Persian','elami' ),
	  'pro' => esc_html__( 'Old Provençal','elami' ),
	  'or' => esc_html__( 'Oriya','elami' ),
	  'om' => esc_html__( 'Oromo','elami' ),
	  'osa' => esc_html__( 'Osage','elami' ),
	  'os' => esc_html__( 'Ossetic','elami' ),
	  'ota' => esc_html__( 'Ottoman Turkish','elami' ),
	  'pal' => esc_html__( 'Pahlavi','elami' ),
	  'pfl' => esc_html__( 'Palatine German','elami' ),
	  'pau' => esc_html__( 'Palauan','elami' ),
	  'pi' => esc_html__( 'Pali','elami' ),
	  'pam' => esc_html__( 'Pampanga','elami' ),
	  'pag' => esc_html__( 'Pangasinan','elami' ),
	  'pap' => esc_html__( 'Papiamento','elami' ),
	  'ps' => esc_html__( 'Pashto','elami' ),
	  'pdc' => esc_html__( 'Pennsylvania German','elami' ),
	  'fa' => esc_html__( 'Persian','elami' ),
	  'phn' => esc_html__( 'Phoenician','elami' ),
	  'pcd' => esc_html__( 'Picard','elami' ),
	  'pms' => esc_html__( 'Piedmontese','elami' ),
	  'pdt' => esc_html__( 'Plautdietsch','elami' ),
	  'pon' => esc_html__( 'Pohnpeian','elami' ),
	  'pl' => esc_html__( 'Polish','elami' ),
	  'pnt' => esc_html__( 'Pontic','elami' ),
	  'pt' => esc_html__( 'Portuguese','elami' ),
	  'prg' => esc_html__( 'Prussian','elami' ),
	  'pa' => esc_html__( 'Punjabi','elami' ),
	  'qu' => esc_html__( 'Quechua','elami' ),
	  'raj' => esc_html__( 'Rajasthani','elami' ),
	  'rap' => esc_html__( 'Rapanui','elami' ),
	  'rar' => esc_html__( 'Rarotongan','elami' ),
	  'rif' => esc_html__( 'Riffian','elami' ),
	  'rgn' => esc_html__( 'Romagnol','elami' ),
	  'ro' => esc_html__( 'Romanian','elami' ),
	  'rm' => esc_html__( 'Romansh','elami' ),
	  'rom' => esc_html__( 'Romany','elami' ),
	  'rof' => esc_html__( 'Rombo','elami' ),
	  'root' => esc_html__( 'Root','elami' ),
	  'rtm' => esc_html__( 'Rotuman','elami' ),
	  'rug' => esc_html__( 'Roviana','elami' ),
	  'rn' => esc_html__( 'Rundi','elami' ),
	  'ru' => esc_html__( 'Russian','elami' ),
	  'rue' => esc_html__( 'Rusyn','elami' ),
	  'rwk' => esc_html__( 'Rwa','elami' ),
	  'ssy' => esc_html__( 'Saho','elami' ),
	  'sah' => esc_html__( 'Sakha','elami' ),
	  'sam' => esc_html__( 'Samaritan Aramaic','elami' ),
	  'saq' => esc_html__( 'Samburu','elami' ),
	  'sm' => esc_html__( 'Samoan','elami' ),
	  'sgs' => esc_html__( 'Samogitian','elami' ),
	  'sad' => esc_html__( 'Sandawe','elami' ),
	  'sg' => esc_html__( 'Sango','elami' ),
	  'sbp' => esc_html__( 'Sangu','elami' ),
	  'sa' => esc_html__( 'Sanskrit','elami' ),
	  'sat' => esc_html__( 'Santali','elami' ),
	  'sc' => esc_html__( 'Sardinian','elami' ),
	  'sas' => esc_html__( 'Sasak','elami' ),
	  'sdc' => esc_html__( 'Sassarese Sardinian','elami' ),
	  'stq' => esc_html__( 'Saterland Frisian','elami' ),
	  'saz' => esc_html__( 'Saurashtra','elami' ),
	  'sco' => esc_html__( 'Scots','elami' ),
	  'gd' => esc_html__( 'Scottish Gaelic','elami' ),
	  'sly' => esc_html__( 'Selayar','elami' ),
	  'sel' => esc_html__( 'Selkup','elami' ),
	  'seh' => esc_html__( 'Sena','elami' ),
	  'see' => esc_html__( 'Seneca','elami' ),
	  'sr' => esc_html__( 'Serbian','elami' ),
	  'sh' => esc_html__( 'Serbo-Croatian','elami' ),
	  'srr' => esc_html__( 'Serer','elami' ),
	  'sei' => esc_html__( 'Seri','elami' ),
	  'ksb' => esc_html__( 'Shambala','elami' ),
	  'shn' => esc_html__( 'Shan','elami' ),
	  'sn' => esc_html__( 'Shona','elami' ),
	  'ii' => esc_html__( 'Sichuan Yi','elami' ),
	  'scn' => esc_html__( 'Sicilian','elami' ),
	  'sid' => esc_html__( 'Sidamo','elami' ),
	  'bla' => esc_html__( 'Siksika','elami' ),
	  'szl' => esc_html__( 'Silesian','elami' ),
	  'zh_Hans' => esc_html__( 'Simplified Chinese','elami' ),
	  'sd' => esc_html__( 'Sindhi','elami' ),
	  'si' => esc_html__( 'Sinhala','elami' ),
	  'sms' => esc_html__( 'Skolt Sami','elami' ),
	  'den' => esc_html__( 'Slave','elami' ),
	  'sk' => esc_html__( 'Slovak','elami' ),
	  'sl' => esc_html__( 'Slovenian','elami' ),
	  'xog' => esc_html__( 'Soga','elami' ),
	  'sog' => esc_html__( 'Sogdien','elami' ),
	  'so' => esc_html__( 'Somali','elami' ),
	  'snk' => esc_html__( 'Soninke','elami' ),
	  'ckb' => esc_html__( 'Sorani Kurdish','elami' ),
	  'azb' => esc_html__( 'South Azerbaijani','elami' ),
	  'nr' => esc_html__( 'South Ndebele','elami' ),
	  'alt' => esc_html__( 'Southern Altai','elami' ),
	  'sma' => esc_html__( 'Southern Sami','elami' ),
	  'st' => esc_html__( 'Southern Sotho','elami' ),
	  'es' => esc_html__( 'Spanish','elami' ),
	  'srn' => esc_html__( 'Sranan Tongo','elami' ),
	  'zgh' => esc_html__( 'Standard Moroccan Tamazight','elami' ),
	  'suk' => esc_html__( 'Sukuma','elami' ),
	  'sux' => esc_html__( 'Sumerian','elami' ),
	  'su' => esc_html__( 'Sundanese','elami' ),
	  'sus' => esc_html__( 'Susu','elami' ),
	  'sw' => esc_html__( 'Swahili','elami' ),
	  'ss' => esc_html__( 'Swati','elami' ),
	  'sv' => esc_html__( 'Swedish','elami' ),
	  'fr_CH' => esc_html__( 'Swiss French','elami' ),
	  'gsw' => esc_html__( 'Swiss German','elami' ),
	  'de_CH' => esc_html__( 'Swiss High German','elami' ),
	  'syr' => esc_html__( 'Syriac','elami' ),
	  'shi' => esc_html__( 'Tachelhit','elami' ),
	  'tl' => esc_html__( 'Tagalog','elami' ),
	  'ty' => esc_html__( 'Tahitian','elami' ),
	  'dav' => esc_html__( 'Taita','elami' ),
	  'tg' => esc_html__( 'Tajik','elami' ),
	  'tly' => esc_html__( 'Talysh','elami' ),
	  'tmh' => esc_html__( 'Tamashek','elami' ),
	  'ta' => esc_html__( 'Tamil','elami' ),
	  'trv' => esc_html__( 'Taroko','elami' ),
	  'twq' => esc_html__( 'Tasawaq','elami' ),
	  'tt' => esc_html__( 'Tatar','elami' ),
	  'te' => esc_html__( 'Telugu','elami' ),
	  'ter' => esc_html__( 'Tereno','elami' ),
	  'teo' => esc_html__( 'Teso','elami' ),
	  'tet' => esc_html__( 'Tetum','elami' ),
	  'th' => esc_html__( 'Thai','elami' ),
	  'bo' => esc_html__( 'Tibetan','elami' ),
	  'tig' => esc_html__( 'Tigre','elami' ),
	  'ti' => esc_html__( 'Tigrinya','elami' ),
	  'tem' => esc_html__( 'Timne','elami' ),
	  'tiv' => esc_html__( 'Tiv','elami' ),
	  'tli' => esc_html__( 'Tlingit','elami' ),
	  'tpi' => esc_html__( 'Tok Pisin','elami' ),
	  'tkl' => esc_html__( 'Tokelau','elami' ),
	  'to' => esc_html__( 'Tongan','elami' ),
	  'fit' => esc_html__( 'Tornedalen Finnish','elami' ),
	  'zh_Hant' => esc_html__( 'Traditional Chinese','elami' ),
	  'tkr' => esc_html__( 'Tsakhur','elami' ),
	  'tsd' => esc_html__( 'Tsakonian','elami' ),
	  'tsi' => esc_html__( 'Tsimshian','elami' ),
	  'ts' => esc_html__( 'Tsonga','elami' ),
	  'tn' => esc_html__( 'Tswana','elami' ),
	  'tcy' => esc_html__( 'Tulu','elami' ),
	  'tum' => esc_html__( 'Tumbuka','elami' ),
	  'aeb' => esc_html__( 'Tunisian Arabic','elami' ),
	  'tr' => esc_html__( 'Turkish','elami' ),
	  'tk' => esc_html__( 'Turkmen','elami' ),
	  'tru' => esc_html__( 'Turoyo','elami' ),
	  'tvl' => esc_html__( 'Tuvalu','elami' ),
	  'tyv' => esc_html__( 'Tuvinian','elami' ),
	  'tw' => esc_html__( 'Twi','elami' ),
	  'kcg' => esc_html__( 'Tyap','elami' ),
	  'udm' => esc_html__( 'Udmurt','elami' ),
	  'uga' => esc_html__( 'Ugaritic','elami' ),
	  'uk' => esc_html__( 'Ukrainian','elami' ),
	  'umb' => esc_html__( 'Umbundu','elami' ),
	  'und' => esc_html__( 'Unknown Language','elami' ),
	  'hsb' => esc_html__( 'Upper Sorbian','elami' ),
	  'ur' => esc_html__( 'Urdu','elami' ),
	  'ug' => esc_html__( 'Uyghur','elami' ),
	  'uz' => esc_html__( 'Uzbek','elami' ),
	  'vai' => esc_html__( 'Vai','elami' ),
	  've' => esc_html__( 'Venda','elami' ),
	  'vec' => esc_html__( 'Venetian','elami' ),
	  'vep' => esc_html__( 'Veps','elami' ),
	  'vi' => esc_html__( 'Vietnamese','elami' ),
	  'vo' => esc_html__( 'Volapük','elami' ),
	  'vro' => esc_html__( 'Võro','elami' ),
	  'vot' => esc_html__( 'Votic','elami' ),
	  'vun' => esc_html__( 'Vunjo','elami' ),
	  'wal' => esc_html__( 'Walamo','elami' ),
	  'wa' => esc_html__( 'Walloon','elami' ),
	  'wae' => esc_html__( 'Walser','elami' ),
	  'war' => esc_html__( 'Waray','elami' ),
	  'was' => esc_html__( 'Washo','elami' ),
	  'guc' => esc_html__( 'Wayuu','elami' ),
	  'cy' => esc_html__( 'Welsh','elami' ),
	  'vls' => esc_html__( 'West Flemish','elami' ),
	  'fy' => esc_html__( 'Western Frisian','elami' ),
	  'mrj' => esc_html__( 'Western Mari','elami' ),
	  'wo' => esc_html__( 'Wolof','elami' ),
	  'wuu' => esc_html__( 'Wu Chinese','elami' ),
	  'xh' => esc_html__( 'Xhosa','elami' ),
	  'hsn' => esc_html__( 'Xiang Chinese','elami' ),
	  'yav' => esc_html__( 'Yangben','elami' ),
	  'yao' => esc_html__( 'Yao','elami' ),
	  'yap' => esc_html__( 'Yapese','elami' ),
	  'ybb' => esc_html__( 'Yemba','elami' ),
	  'yi' => esc_html__( 'Yiddish','elami' ),
	  'yo' => esc_html__( 'Yoruba','elami' ),
	  'zap' => esc_html__( 'Zapotec','elami' ),
	  'dje' => esc_html__( 'Zarma','elami' ),
	  'zza' => esc_html__( 'Zaza','elami' ),
	  'zea' => esc_html__( 'Zeelandic','elami' ),
	  'zen' => esc_html__( 'Zenaga','elami' ),
	  'za' => esc_html__( 'Zhuang','elami' ),
	  'gbz' => esc_html__( 'Zoroastrian Dari','elami' ),
	  'zu' => esc_html__( 'Zulu','elami' ),
	  'zun' => esc_html__( 'Zuni','elami' ),
	);
}

/**
 * Return array of translated country names
 *
 * @return array
 * @author Willem Prins | Somtijds
 * @link via: https://github.com/umpirsky/country-list/blob/master/data/en_GB/country.php
 **/
// function elami_country_list() {
// 	return [
//     'AF' => esc_html__( 'Afghanistan','elami' ),
//     'AL' => esc_html__( 'Albania','elami' ),
//     'DZ' => esc_html__( 'Algeria','elami' ),
//     'AS' => esc_html__( 'American Samoa','elami' ),
//     'AD' => esc_html__( 'Andorra','elami' ),
//     'AO' => esc_html__( 'Angola','elami' ),
//     'AI' => esc_html__( 'Anguilla','elami' ),
//     'AQ' => esc_html__( 'Antarctica','elami' ),
//     'AG' => esc_html__( 'Antigua And Barbuda','elami' ),
//     'AR' => esc_html__( 'Argentina','elami' ),
//     'AM' => esc_html__( 'Armenia','elami' ),
//     'AW' => esc_html__( 'Aruba','elami' ),
//     'AU' => esc_html__( 'Australia','elami' ),
//     'AT' => esc_html__( 'Austria','elami' ),
//     'AZ' => esc_html__( 'Azerbaijan','elami' ),
//     'BS' => esc_html__( 'Bahamas','elami' ),
//     'BH' => esc_html__( 'Bahrain','elami' ),
//     'BD' => esc_html__( 'Bangladesh','elami' ),
//     'BB' => esc_html__( 'Barbados','elami' ),
//     'BY' => esc_html__( 'Belarus','elami' ),
//     'BE' => esc_html__( 'Belgium','elami' ),
//     'BZ' => esc_html__( 'Belize','elami' ),
//     'BJ' => esc_html__( 'Benin','elami' ),
//     'BM' => esc_html__( 'Bermuda','elami' ),
//     'BT' => esc_html__( 'Bhutan','elami' ),
//     'BO' => esc_html__( 'Bolivia','elami' ),
//     'BA' => esc_html__( 'Bosnia And Herzegovina','elami' ),
//     'BW' => esc_html__( 'Botswana','elami' ),
//     'BV' => esc_html__( 'Bouvet Island','elami' ),
//     'BR' => esc_html__( 'Brazil','elami' ),
//     'IO' => esc_html__( 'British Indian Ocean Territory','elami' ),
//     'BN' => esc_html__( 'Brunei Darussalam','elami' ),
//     'BG' => esc_html__( 'Bulgaria','elami' ),
//     'BF' => esc_html__( 'Burkina Faso','elami' ),
//     'BI' => esc_html__( 'Burundi','elami' ),
//     'KH' => esc_html__( 'Cambodia','elami' ),
//     'CM' => esc_html__( 'Cameroon','elami' ),
//     'CA' => esc_html__( 'Canada','elami' ),
//     'CV' => esc_html__( 'Cape Verde','elami' ),
//     'KY' => esc_html__( 'Cayman Islands','elami' ),
//     'CF' => esc_html__( 'Central African Republic','elami' ),
//     'TD' => esc_html__( 'Chad','elami' ),
//     'CL' => esc_html__( 'Chile','elami' ),
//     'CN' => esc_html__( 'China','elami' ),
//     'CX' => esc_html__( 'Christmas Island','elami' ),
//     'CC' => esc_html__( 'Cocos (keeling) Islands','elami' ),
//     'CO' => esc_html__( 'Colombia','elami' ),
//     'KM' => esc_html__( 'Comoros','elami' ),
//     'CG' => esc_html__( 'Congo','elami' ),
//     'CD' => esc_html__( 'Congo, The Democratic Republic Of The','elami' ),
//     'CK' => esc_html__( 'Cook Islands','elami' ),
//     'CR' => esc_html__( 'Costa Rica','elami' ),
//     'CI' => esc_html__( 'Cote D\'ivoire','elami' ),
//     'HR' => esc_html__( 'Croatia','elami' ),
//     'CU' => esc_html__( 'Cuba','elami' ),
//     'CY' => esc_html__( 'Cyprus','elami' ),
//     'CZ' => esc_html__( 'Czech Republic','elami' ),
//     'DK' => esc_html__( 'Denmark','elami' ),
//     'DJ' => esc_html__( 'Djibouti','elami' ),
//     'DM' => esc_html__( 'Dominica','elami' ),
//     'DO' => esc_html__( 'Dominican Republic','elami' ),
//     'TP' => esc_html__( 'East Timor','elami' ),
//     'EC' => esc_html__( 'Ecuador','elami' ),
//     'EG' => esc_html__( 'Egypt','elami' ),
//     'SV' => esc_html__( 'El Salvador','elami' ),
//     'GQ' => esc_html__( 'Equatorial Guinea','elami' ),
//     'ER' => esc_html__( 'Eritrea','elami' ),
//     'EE' => esc_html__( 'Estonia','elami' ),
//     'ET' => esc_html__( 'Ethiopia','elami' ),
//     'FK' => esc_html__( 'Falkland Islands (malvinas)','elami' ),
//     'FO' => esc_html__( 'Faroe Islands','elami' ),
//     'FJ' => esc_html__( 'Fiji','elami' ),
//     'FI' => esc_html__( 'Finland','elami' ),
//     'FR' => esc_html__( 'France','elami' ),
//     'GF' => esc_html__( 'French Guiana','elami' ),
//     'PF' => esc_html__( 'French Polynesia','elami' ),
//     'TF' => esc_html__( 'French Southern Territories','elami' ),
//     'GA' => esc_html__( 'Gabon','elami' ),
//     'GM' => esc_html__( 'Gambia','elami' ),
//     'GE' => esc_html__( 'Georgia','elami' ),
//     'DE' => esc_html__( 'Germany','elami' ),
//     'GH' => esc_html__( 'Ghana','elami' ),
//     'GI' => esc_html__( 'Gibraltar','elami' ),
//     'GR' => esc_html__( 'Greece','elami' ),
//     'GL' => esc_html__( 'Greenland','elami' ),
//     'GD' => esc_html__( 'Grenada','elami' ),
//     'GP' => esc_html__( 'Guadeloupe','elami' ),
//     'GU' => esc_html__( 'Guam','elami' ),
//     'GT' => esc_html__( 'Guatemala','elami' ),
//     'GN' => esc_html__( 'Guinea','elami' ),
//     'GW' => esc_html__( 'Guinea-bissau','elami' ),
//     'GY' => esc_html__( 'Guyana','elami' ),
//     'HT' => esc_html__( 'Haiti','elami' ),
//     'HM' => esc_html__( 'Heard Island And Mcdonald Islands','elami' ),
//     'VA' => esc_html__( 'Holy See (vatican City State)','elami' ),
//     'HN' => esc_html__( 'Honduras','elami' ),
//     'HK' => esc_html__( 'Hong Kong','elami' ),
//     'HU' => esc_html__( 'Hungary','elami' ),
//     'IS' => esc_html__( 'Iceland','elami' ),
//     'IN' => esc_html__( 'India','elami' ),
//     'ID' => esc_html__( 'Indonesia','elami' ),
//     'IR' => esc_html__( 'Iran, Islamic Republic Of','elami' ),
//     'IQ' => esc_html__( 'Iraq','elami' ),
//     'IE' => esc_html__( 'Ireland','elami' ),
//     'IL' => esc_html__( 'Israel','elami' ),
//     'IT' => esc_html__( 'Italy','elami' ),
//     'JM' => esc_html__( 'Jamaica','elami' ),
//     'JP' => esc_html__( 'Japan','elami' ),
//     'JO' => esc_html__( 'Jordan','elami' ),
//     'KZ' => esc_html__( 'Kazakstan','elami' ),
//     'KE' => esc_html__( 'Kenya','elami' ),
//     'KI' => esc_html__( 'Kiribati','elami' ),
//     'KP' => esc_html__( 'Korea, Democratic People\'s Republic Of','elami' ),
//     'KR' => esc_html__( 'Korea, Republic Of','elami' ),
//     'KV' => esc_html__( 'Kosovo','elami' ),
//     'KW' => esc_html__( 'Kuwait','elami' ),
//     'KG' => esc_html__( 'Kyrgyzstan','elami' ),
//     'LA' => esc_html__( 'Lao People\'s Democratic Republic','elami' ),
//     'LV' => esc_html__( 'Latvia','elami' ),
//     'LB' => esc_html__( 'Lebanon','elami' ),
//     'LS' => esc_html__( 'Lesotho','elami' ),
//     'LR' => esc_html__( 'Liberia','elami' ),
//     'LY' => esc_html__( 'Libyan Arab Jamahiriya','elami' ),
//     'LI' => esc_html__( 'Liechtenstein','elami' ),
//     'LT' => esc_html__( 'Lithuania','elami' ),
//     'LU' => esc_html__( 'Luxembourg','elami' ),
//     'MO' => esc_html__( 'Macau','elami' ),
//     'MK' => esc_html__( 'Macedonia, The Former Yugoslav Republic Of','elami' ),
//     'MG' => esc_html__( 'Madagascar','elami' ),
//     'MW' => esc_html__( 'Malawi','elami' ),
//     'MY' => esc_html__( 'Malaysia','elami' ),
//     'MV' => esc_html__( 'Maldives','elami' ),
//     'ML' => esc_html__( 'Mali','elami' ),
//     'MT' => esc_html__( 'Malta','elami' ),
//     'MH' => esc_html__( 'Marshall Islands','elami' ),
//     'MQ' => esc_html__( 'Martinique','elami' ),
//     'MR' => esc_html__( 'Mauritania','elami' ),
//     'MU' => esc_html__( 'Mauritius','elami' ),
//     'YT' => esc_html__( 'Mayotte','elami' ),
//     'MX' => esc_html__( 'Mexico','elami' ),
//     'FM' => esc_html__( 'Micronesia, Federated States Of','elami' ),
//     'MD' => esc_html__( 'Moldova, Republic Of','elami' ),
//     'MC' => esc_html__( 'Monaco','elami' ),
//     'MN' => esc_html__( 'Mongolia','elami' ),
//     'MS' => esc_html__( 'Montserrat','elami' ),
//     'ME' => esc_html__( 'Montenegro','elami' ),
//     'MA' => esc_html__( 'Morocco','elami' ),
//     'MZ' => esc_html__( 'Mozambique','elami' ),
//     'MM' => esc_html__( 'Myanmar','elami' ),
//     'NA' => esc_html__( 'Namibia','elami' ),
//     'NR' => esc_html__( 'Nauru','elami' ),
//     'NP' => esc_html__( 'Nepal','elami' ),
//     'NL' => esc_html__( 'Netherlands','elami' ),
//     'AN' => esc_html__( 'Netherlands Antilles','elami' ),
//     'NC' => esc_html__( 'New Caledonia','elami' ),
//     'NZ' => esc_html__( 'New Zealand','elami' ),
//     'NI' => esc_html__( 'Nicaragua','elami' ),
//     'NE' => esc_html__( 'Niger','elami' ),
//     'NG' => esc_html__( 'Nigeria','elami' ),
//     'NU' => esc_html__( 'Niue','elami' ),
//     'NF' => esc_html__( 'Norfolk Island','elami' ),
//     'MP' => esc_html__( 'Northern Mariana Islands','elami' ),
//     'NO' => esc_html__( 'Norway','elami' ),
//     'OM' => esc_html__( 'Oman','elami' ),
//     'PK' => esc_html__( 'Pakistan','elami' ),
//     'PW' => esc_html__( 'Palau','elami' ),
//     'PS' => esc_html__( 'Palestinian Territory, Occupied','elami' ),
//     'PA' => esc_html__( 'Panama','elami' ),
//     'PG' => esc_html__( 'Papua New Guinea','elami' ),
//     'PY' => esc_html__( 'Paraguay','elami' ),
//     'PE' => esc_html__( 'Peru','elami' ),
//     'PH' => esc_html__( 'Philippines','elami' ),
//     'PN' => esc_html__( 'Pitcairn','elami' ),
//     'PL' => esc_html__( 'Poland','elami' ),
//     'PT' => esc_html__( 'Portugal','elami' ),
//     'PR' => esc_html__( 'Puerto Rico','elami' ),
//     'QA' => esc_html__( 'Qatar','elami' ),
//     'RE' => esc_html__( 'Reunion','elami' ),
//     'RO' => esc_html__( 'Romania','elami' ),
//     'RU' => esc_html__( 'Russian Federation','elami' ),
//     'RW' => esc_html__( 'Rwanda','elami' ),
//     'SH' => esc_html__( 'Saint Helena','elami' ),
//     'KN' => esc_html__( 'Saint Kitts And Nevis','elami' ),
//     'LC' => esc_html__( 'Saint Lucia','elami' ),
//     'PM' => esc_html__( 'Saint Pierre And Miquelon','elami' ),
//     'VC' => esc_html__( 'Saint Vincent And The Grenadines','elami' ),
//     'WS' => esc_html__( 'Samoa','elami' ),
//     'SM' => esc_html__( 'San Marino','elami' ),
//     'ST' => esc_html__( 'Sao Tome And Principe','elami' ),
//     'SA' => esc_html__( 'Saudi Arabia','elami' ),
//     'SN' => esc_html__( 'Senegal','elami' ),
//     'RS' => esc_html__( 'Serbia','elami' ),
//     'SC' => esc_html__( 'Seychelles','elami' ),
//     'SL' => esc_html__( 'Sierra Leone','elami' ),
//     'SG' => esc_html__( 'Singapore','elami' ),
//     'SK' => esc_html__( 'Slovakia','elami' ),
//     'SI' => esc_html__( 'Slovenia','elami' ),
//     'SB' => esc_html__( 'Solomon Islands','elami' ),
//     'SO' => esc_html__( 'Somalia','elami' ),
//     'ZA' => esc_html__( 'South Africa','elami' ),
//     'GS' => esc_html__( 'South Georgia And The South Sandwich Islands','elami' ),
//     'ES' => esc_html__( 'Spain','elami' ),
//     'LK' => esc_html__( 'Sri Lanka','elami' ),
//     'SD' => esc_html__( 'Sudan','elami' ),
//     'SR' => esc_html__( 'Suriname','elami' ),
//     'SJ' => esc_html__( 'Svalbard And Jan Mayen','elami' ),
//     'SZ' => esc_html__( 'Swaziland','elami' ),
//     'SE' => esc_html__( 'Sweden','elami' ),
//     'CH' => esc_html__( 'Switzerland','elami' ),
//     'SY' => esc_html__( 'Syrian Arab Republic','elami' ),
//     'TW' => esc_html__( 'Taiwan, Province Of China','elami' ),
//     'TJ' => esc_html__( 'Tajikistan','elami' ),
//     'TZ' => esc_html__( 'Tanzania, United Republic Of','elami' ),
//     'TH' => esc_html__( 'Thailand','elami' ),
//     'TG' => esc_html__( 'Togo','elami' ),
//     'TK' => esc_html__( 'Tokelau','elami' ),
//     'TO' => esc_html__( 'Tonga','elami' ),
//     'TT' => esc_html__( 'Trinidad And Tobago','elami' ),
//     'TN' => esc_html__( 'Tunisia','elami' ),
//     'TR' => esc_html__( 'Turkey','elami' ),
//     'TM' => esc_html__( 'Turkmenistan','elami' ),
//     'TC' => esc_html__( 'Turks And Caicos Islands','elami' ),
//     'TV' => esc_html__( 'Tuvalu','elami' ),
//     'UG' => esc_html__( 'Uganda','elami' ),
//     'UA' => esc_html__( 'Ukraine','elami' ),
//     'AE' => esc_html__( 'United Arab Emirates','elami' ),
//     'GB' => esc_html__( 'United Kingdom','elami' ),
//     'US' => esc_html__( 'United States','elami' ),
//     'UM' => esc_html__( 'United States Minor Outlying Islands','elami' ),
//     'UY' => esc_html__( 'Uruguay','elami' ),
//     'UZ' => esc_html__( 'Uzbekistan','elami' ),
//     'VU' => esc_html__( 'Vanuatu','elami' ),
//     'VE' => esc_html__( 'Venezuela','elami' ),
//     'VN' => esc_html__( 'Viet Nam','elami' ),
//     'VG' => esc_html__( 'Virgin Islands, British','elami' ),
//     'VI' => esc_html__( 'Virgin Islands, U.s.','elami' ),
//     'WF' => esc_html__( 'Wallis And Futuna','elami' ),
//     'EH' => esc_html__( 'Western Sahara','elami' ),
//     'YE' => esc_html__( 'Yemen','elami' ),
//     'ZM' => esc_html__( 'Zambia','elami' ),
//     'ZW' => esc_html__( 'Zimbabwe','elami' ),
// 	];
// }

function elami_build_svg( $svg_src, $fallback = false) {
	if ( ! is_string( $svg_src ) || '' === $svg_src ) {
		return;
	}
	if ( ! is_readable( $svg_src ) ) {
		return;
	}
	$svg = file_get_contents( $svg_src );
	if ( $fallback ) {
		$png_src = str_replace( 'svg', 'png', $svg_src );
		$svg = elami_insert_svg_fallback( $svg, $png_src );
	}
	return $svg;
}

function elami_insert_svg_fallback( $svg, $png_src ) {
	// Add png fallback if available.
	if ( ! is_readable( $png_src ) ) {
		return $svg;
	} else {
		$png_insert = '<image src="' . $png_src . '" xlink:href=""></svg>';
		$svg = substr_replace( $svg, $png_insert, -6 );
		return $svg;
	}
}

/**
 * Retrieves the attachment ID from the file 
 *
 * @return void
 * @author Pippins Plugins
 * @link https://pippinsplugins.com/retrieve-attachment-id-from-image-url/
 **/
function elami_get_image_id($image_url) {
	global $wpdb;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0]; 
}

function elami_the_child_page_buttons( $string = '' ) {
    $args = array(
			'post_parent' => get_the_ID(),
			'post_type'   => 'page',
			'numberposts' => 5,
			'post_status' => 'any',
			'orderby'     => 'menu_order',
			'order'       => 'asc',
		);
	$child_pages = get_posts( $args );
	if ( empty( $child_pages ) ) {
		return;
	}
	foreach ( $child_pages as $child_page ) : ?>
		<?php if ( ! empty( $string ) ) {
			$button_text = wp_sprintf( $string, strtolower( $child_page->post_title ) );
		} else {
			$button_text = $child_page->post_title;
		}
		?>
		<a class="button" href="<?php echo esc_url( get_permalink( $child_page->ID ) ); ?>">
			<?php echo esc_html( $button_text ); ?>
		</a>
	<?php endforeach;

}
/**
 * Echo the artists for this title.
 * @param int Post_ID
 * @return void
 * @author Willem Prins | Somtijds
 **/
function elami_the_book_artists( $id = null ) {
	if ( empty( $id ) ) {
		$id = get_the_id();
	}
	$artists = array();
	$authors = get_post_meta( $id, 'elami_book_attached_authors', true );
	$illustrators = get_post_meta( $id, 'elami_book_attached_illustrators', true );
	if ( empty( $authors ) && empty( $illustrators ) ) {
		return;
	}
	if ( $authors ) {
		$artists = array_merge( $artists, $authors );
	}
	if ( $illustrators ) {
		$artists = array_merge( $artists, $illustrators );
	}
	$artists = array_unique( $artists );

	echo '<ul class="artists">';
	foreach ( $artists as $artist ) {
		$artist_obj = get_post( $artist );
		if ( ! empty( $artist_obj ) && 'artist' === $artist_obj->post_type ) {

			if ( is_singular( 'book' ) ) {
				$link = get_the_permalink ( $artist_obj->ID );
				echo '<a href="' . $link . '">';
			}; ?>
			
			<li class="artist">
				<?php esc_html_e( $artist_obj->post_title ); ?>
			</li>

			<?php if ( is_singular( 'book' ) ) {
				echo '</a>';
			};

		};
	}
	echo '</ul>';
}

/**
 * Detect whether the post is the last in the query
 *
 * @return  Willem Prins | Somtijdsvoid
 * @author 
 **/
function elami_the_last_post_class() {
	global $wp_query;
	if ( ( $wp_query->current_post + 1 ) == ( $wp_query->post_count ) ) {
		esc_attr_e(' end');
	}
}

/**
 * undocumented function
 *
 * @return void
 * @author 
 **/
function elami_the_contact_link( $user_id = 2, $button = true ) {
	$contactme = get_userdata( $user_id );
	if ( ! $contactme instanceof WP_User ) {
		return;
	}
	if ( function_exists( 'eae_encode_str' ) && ! empty( $contactme->user_email ) ) {		
		$mem = eae_encode_str( $contactme->user_email );
		if ( $button ) {
			echo '<a class="button" href="mailto:' . $mem . '">' . esc_html__( 'Enquiries? Get in touch!','elami' ) . '</a>';
		} else {
			echo 'mailto:' . $mem;
		}
	}
}

/**
 * Check if an artist_ID matches an artist
 *
 * @return int ID to check for
 * @return WP_Post object or false
 * @author Willem Prins | Somtijds
 **/
function elami_artist_exists( $artist_id ) {
	$artist = get_post( $artist_id );
	if ( $artist instanceof WP_Post && 'artist' === $artist->post_type ) {
		return $artist;
	} else {
		return false;
	}
}

/**
 * Pass in a taxonomy value that is supported by WP's `get_taxonomy`
 * and you will get back the url to the archive view.
 * @param $taxonomy string|int
 * @return string
 * @link http://wordpress.stackexchange.com/a/198558
 */
function elami_get_taxonomy_archive_link( $taxonomy ) {
  $tax = get_taxonomy( $taxonomy ) ;
  return get_bloginfo( 'url' ) . '/' . $tax->rewrite['slug'];
}


function elami_technical_details( $book_meta ) {
    $id = get_the_ID();
	$output = '';
	$book_details_inline = array('format','pages','size');
	$inline_values = [];
	foreach( $book_details_inline as $detail ) {
		if ( 'format' === $detail ) {
			$format = get_the_terms( $id, 'format' );
			if ( ! empty( $format ) ) {
				$inline_values[] = $format[0]->name;
			}
		} elseif ( ! empty( $book_meta['elami_book_details_' . $detail ]) ) {
			$inline_values[] = $book_meta['elami_book_details_' . $detail ][0];
		}
	}
	if ( count( $inline_values ) > 0 ) {
		$output .= '<li class="elami_book_details__item">' . esc_html( implode( ' | ', $inline_values ) ) . '</li>';
	}

	$book_details_table = array(
		'original_title'   => __('Original title', 'elami' ),
		'publisher'        => __('Publisher', 'elami'),
		'rights_available' => __('Rights available', 'elami' ),
	);

	foreach( $book_details_table as $key => $label ) {
		if ( 'publisher' === $key ) {
			$publishers = get_the_terms( $id, 'publisher' );
			if ( empty( $publishers ) ) {
				continue;
			}
			$stored_value = esc_html( $publishers[0]->name );
		} elseif ( 'rights_available' === $key ) {
		    $rights = get_the_term_list( $id, 'available_rights' );
            $stored_rights_meta = $book_meta['elami_book_details_rights_available'][0];
		    if ( empty ( $rights ) ) {
                $stored_value = elami_transfer_rights_meta_to_term($id, $stored_rights_meta);
            } else {
                $stored_value = $rights;
            }
            if ( empty( $stored_value ) ) {
                continue;
            }
        } else {
			if ( empty( $book_meta['elami_book_details_' . $key ]) ) {
				continue;
			}
			$stored_value = esc_html( $book_meta['elami_book_details_' . $key][0] );
		}
		// Render output.
		$output .= '<li class="elami_book_details__item elami_book_details__item--' . $key . '"><label>' . $label . ': ' . '</label>' . '<span>' . $stored_value . '</span></li>';
	}
	if ( '' !== $output ) {
		return '<ul class="entry-details elami_book_details">' . $output . '</ul>';
	}
}

/**
 * Helper function to gradually remove old post meta values in favour of the new taxonomy
 * @param int $id Post ID
 * @param string $stored_rights_meta String or empty value from old post meta value
 * @return void | string
 */
function elami_transfer_rights_meta_to_term( $id, $stored_rights_meta ) {
    if ( empty( $stored_rights_meta ) ) {
        return;
    }
    $transfer = wp_set_post_terms( $id, $stored_rights_meta, 'available_rights', false );
    if ( ! is_array( $transfer ) || ! (int) $transfer[0] ) {
        return;
    }
    $term = get_term_by( 'id', $transfer[0], 'available_rights' );
    if ( ! $term instanceof WP_Term ) {
        return;
    }
    delete_post_meta( $id, 'elami_book_details_rights_available' );
    return $term->name;
}

function elami_the_book_details() {
	$id = get_the_id();
	if ( empty( $id ) ) {
		return;
	}
	$book_meta = get_post_meta( $id );
	if ( empty( $book_meta ) ) {
		return;
	}
	$output .= elami_technical_details( $book_meta );
	if ( ! empty( $book_meta['elami_book_rights_sold_countries'] ) ) {
		$output .= elami_rights_sold( maybe_unserialize( $book_meta['elami_book_rights_sold_countries'][0] ) );
	}
	if ( '' !== $output ) {
		echo $output;
	}
}

function elami_implode_key_value( $array, $keys ) {
	$result_array = [];
	foreach( $keys as $key ) {
		if ( array_key_exists( $key, $array ) && ! empty ( $array[ $key ] ) ) {
			$result_array[] = $array[ $key ];
		}
	}
	if ( ! empty( $result_array ) ) {
		return implode( ', ', $result_array );
	}
}

function elami_rights_sold( $rights_sold ) {
	$output = '';
	foreach( $rights_sold as $publisher ) {
		if ( empty( $publisher['rights_sold_publisher'] ) ) {
			continue;
		}
		if ( ! empty( $publisher[ 'rights_sold_languages' ] ) ) {
			$languages = (array) $publisher['rights_sold_languages'];
			$language_string = esc_html( elami_implode_key_value( elami_language_list(), $languages ) );
		}
		$output .= '<li class="elami_book_details__item elami_book_details__item"><span class="publisher">' . esc_html( $publisher['rights_sold_publisher'] ) . '</span>';
		if ( ! empty( $language_string ) || ! empty( $publisher[ 'rights_sold_other_info' ] ) ) {
			$output .= ' (';
		}
		if ( ! empty( $language_string ) ) {
			$output .= '<span class="languages">' . $language_string . '</span>';
		}
		if ( ! empty( $publisher[ 'rights_sold_other_info' ] ) ) {
			$output .= ' - <span class="info">' . esc_html( $publisher[ 'rights_sold_other_info' ] ) . '</span>';
		}
		if ( ! empty( $language_string ) || ! empty( $publisher[ 'rights_sold_other_info' ] ) ) {
			$output .= ')';
		}
		$output .= '</li>';
	}
	if ( '' !== $output ) {
		$output = ' <label class="elami_book_details-header">' . __( 'Rights sold:', 'elami' ) . '</label><ul class="entry-details elami_book_rights_sold">' . $output . '</ul>';
		return $output;
	}
}

function elami_the_representation_subheader() {
	if ( ! is_archive() ) {
		return;
	}
	if ( 'on' === get_post_meta( get_the_ID(), 'elami_artist_management', true ) ) {
		$services = get_page_by_title( 'Services' );
		if ( $services instanceof WP_Post ) {
			$services_link = get_the_permalink( $services->ID );
		}
		_e( 'Representation by élami agency','elami' );
		if ( isset( $services_link ) ) {
			$output = '<small class="services-hint"><a href="' . esc_url( $services_link ) . '" ';
			$output .= 'title="' . __('Find out more about our services for artists','elami' ) . '">';
			$output .= __( "?",'elami' ) . '</a></small>';
			echo $output;
		}
	}
}

function elami_the_grid_column_class() {
	if ( ! is_single() ) {
		echo esc_attr( 'xxlarge-2 large-3 medium-4 small-6 columns' );
	} else {
		echo esc_attr( 'xxlarge-3 large-4 medium-6 small-6 columns' );
	}
}


function elami_the_taxonomy_buttons( $taxo ) { ?>

	<nav class="breadcrumbs-bar__taxonomies">
        <?php foreach( $taxo as $taxo_item ) : ?>
		<ul>
			
			<?php $buttons = get_terms( array( 
				'taxonomy' => $taxo_item,
				'hide_empty' => true,
			) );
			foreach( $buttons as $button ) : ?>

				<?php if ( is_tax( $taxo_item, $button->slug ) ) {
					continue;
				} ?>
                <?php if ( 'available_rights' === $taxo_item ) {
                    $title_string = wp_sprintf( __( 'Show me all the titles with these rights available: %s', 'elami' ), $button->name );
                } else {
                    $title_string = wp_sprintf( __( 'Show me everything from: %s', 'elami' ), $button->name );
                }; ?>

				<li><a class="button" href="<?php echo esc_url( get_term_link( $button ) ); ?>" title="<?php echo esc_attr( $title_string ); ?>"><?php echo esc_html( $button->name ); ?></a></li>

			<?php endforeach; ?>

		</ul>
        <?php endforeach; ?>

	</nav>

<?php }

function elami_get_publisher_term_ids() {
	$publisher_terms = get_terms( array( 
		'taxonomy' => 'publisher',
		'hide_empty' => true,
	) ); 
	$publisher_term_ids = array();
	foreach ( $publisher_terms as $publisher_term ) {
		if ( is_int( $publisher_term->term_id ) ) {
			$publisher_term_ids[] = $publisher_term->term_id;
		}
	}
	return $publisher_term_ids;
}


function elami_find_books_for_this_author( $id ) {
	
	if ( empty( $id ) || ! is_integer( $id ) ) {
		return;
	}

	$args = array(
		'post_type'  => 'book',
		'post_status' => 'publish',
		'meta_query' => array(
			'relation' => 'OR',
			array(
				'key'   => 'elami_book_attached_authors',
				'value' => '"' . $id . '"',
				'compare' => 'LIKE',
			),
			array(
				'key'   => 'elami_book_attached_illustrators',
				'value' => '"' . $id . '"',
				'compare' => 'LIKE',
			),
		),
	);
	$books_query = new WP_Query( $args );

	
	return $books_query->have_posts();

}

/**
 * Find out whether an author has a book
 *
 * Only works in the loop, using a transient to offload the database
 *
 * @return type
 * @throws conditon
 **/
function elami_has_books() {

	$id = get_the_id();
	$sane_title = sanitize_title( get_the_title() );
	$authors_have_books = get_transient( 'elami_authors_have_books' );

	if ( ! empty( $authors_have_books ) 
		&& is_array( $authors_have_books )
		&& array_key_exists( $sane_title, $authors_have_books ) ) {
		return $authors_have_books[$sane_title];
	}

	if ( empty( $authors_have_books ) || ! is_array( $authors_have_books ) ) {
		$authors_have_books = array();
	}

	if ( ! array_key_exists( $sane_title, $authors_have_books ) ) {
		$this_author_has_books = elami_find_books_for_this_author( $id );
	}

	if ( ! isset( $this_author_has_books ) ) {
		return;
	}

	$authors_have_books[ $sane_title ] = $this_author_has_books;
	set_transient( 'elami_authors_have_books', $authors_have_books, 1 * HOUR_IN_SECONDS );
	return $this_author_has_books;

}

/**
 * Find out whether the author is represented by élami agency
 *
 *
 * @return bool
 **/
function elami_is_under_management() {

    $id = get_the_id();
    return get_post_meta( $id, 'elami_artist_management', true ) === 'on';

}