<?php
	
// Adding WP Functions & Theme Support
function elami_theme_support() {

	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );
	
	// Default thumbnail size
	set_post_thumbnail_size(125, 125, true);

	// Add RSS Support
	//add_theme_support( 'automatic-feed-links' );
	
	// Add Support for WP Controlled Title Tag
	add_theme_support( 'title-tag' );
	
	// Add HTML5 Support
	// add_theme_support( 'html5', 
	//          array( 
	//          	'comment-list', 
	//          	'comment-form', 
	//          	'search-form', 
	//          ) 
	// );

	// Add custom background support
	add_theme_support( 'custom-background', apply_filters( 'mcq_filter.theme_support_custom_background', array(
		'default-color'          => '#FFFFFF',
		'default-repeat'         => 'no-repeat',
		'default-position-x'     => 'center',
		'default-position-y'     => 'center',
		'default-size'           => 'cover',
		'default-attachment'     => 'fixed',
	) ) );
		
} /* end theme support */

add_action( 'after_setup_theme', 'elami_bct_theme_setup' );

function elami_bct_theme_setup() {
    add_theme_support( 'breadcrumb-trail' );
}