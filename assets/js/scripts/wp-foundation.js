/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='flex-video'/>");

	// Make sure the other responsive menu is closed when one opens
	jQuery('.menu-toggle a').on('click.zf.responsiveToggle', function( e ) {
		var toggle = jQuery(this).parent().data('responsiveToggle');
		if ( toggle === 'menu-main-menu' ) {
			jQuery('#menu-mobile-language-menu').hide();
		}
		if ( toggle === 'menu-mobile-language-menu' ) {
			jQuery('#menu-main-menu').hide();
		}
	});

	// Make sure the other responsive menu is closed when one opens
	jQuery('#slider .slide').on('on.zf.toggle', function( e ) {
		var toggled = jQuery(this).attr('id');
		var slides = jQuery('#slider .slide');
		var excerpts = jQuery('#overview .article__excerpt');
		slides.each(function() {
			if ( jQuery(this).attr('id') != toggled ) {
				jQuery(this).removeClass('active');
			}
		});

	});


});
