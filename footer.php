		<footer class="footer" role="contentinfo">

			<section class="page__footer__nav">
				<nav role="navigation">
					<?php elami_footer_links(); ?>
				</nav>
			</section>
			<section class="page__footer__widgets">
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/png/elami-logo.png'; ?>" alt="élami agency logo" />

				<?php get_template_part( 'parts/content', 'footer' ); ?>

			</section>

			<section class="page__footer__copyright">
				<?php if ( is_front_page() ) : ?>
					<?php get_template_part( 'parts/content', 'features-bar'); ?>
		 		<?php endif; ?>
				<span class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</span>
			</section>

		</footer> <!-- end .footer -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->
