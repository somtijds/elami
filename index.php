<?php get_header(); ?>

<main id="content" role="main">

	<?php if ( is_home() ) : ?>
		<?php get_template_part('parts/content-home-feature'); ?>
	<?php elseif (have_posts()) : while (have_posts()) : the_post(); ?>

		<!-- To see additional archive styles, visit the /parts directory -->
		<?php get_template_part( 'parts/loop', 'archive' ); ?>

	<?php endwhile; ?>

		<?php elami_page_navi(); ?>

	<?php else : ?>

		<?php get_template_part( 'parts/content', 'missing' ); ?>

	<?php endif; ?>

</main> <!-- end #content -->

<?php get_footer(); ?>
