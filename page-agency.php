<?php
/**
 * Template name: Agency
 **/
?>

<?php get_header(); ?>

<main  id="content" role="main" >

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part( 'parts/loop', 'page-agency' ); ?>

	<?php endwhile; endif; ?>

</main> <!-- end #main -->

<?php get_footer(); ?>
