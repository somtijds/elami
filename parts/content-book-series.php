<div class="section-wrapper">

	<aside class="row">

		<section class="elami_book_series_info large-3 medium-4 small-12 columns">

			<header class="section__header">
				<h3><?php _e( 'Related series', 'elami' ); ?></h3>
			</header>

			<ul class="entry-details elami_book_details">

				<?php foreach ( $series as $single_series ) : ?>
						
						<h4><?php echo esc_html( $single_series->name ); ?></h4>
						
						<?php if ( ! empty( $single_series->description ) ) : ?>

							<p><?php echo esc_html( $single_series->description ); ?></p>

						<?php endif; ?>

						<a class="button"
						   href="<?php echo get_term_link( $series[0] ); ?>" 
						   title="<?php _e( 'Show me all the books from this series','elami' ); ?>">
							<?php _e('Show me all the books!','elami') ; ?>
					   </a>

					</li>

				<?php endforeach; ?>

			</ul>

		</section>

<?php $series_term_ids = array();
	foreach ( $series as $single_series ) {
		$series_term_ids[] = $single_series->term_id;
	}
	if ( ! empty( $series_term_ids ) ) {
		$args = array(
			'post_type'    => 'book',
			'post_status'  => 'publish',
			'number_posts' => 4,
			'post__not_in' => array( get_the_ID() ),
			'tax_query' => array(
				array(
					'taxonomy' => 'series',
					'terms'	   => $series_term_ids
				)
			),
		);
		$series_query = new WP_Query( $args );
	}

	if ( isset( $series_query ) && $series_query->have_posts() ) : ?>

		<section class="elami_book_also-from-this-series large-9 medium-8 small-12 columns">

			<header class="section__header">
				<h3><?php _e( 'Related books', 'elami' ); ?></h3>
			</header>

			<div class="grid">

				<div class="row" data-equalizer data-equalize-by-row="true">

					<?php while ( $series_query->have_posts() ) : ?>

						<?php $series_query->the_post(); ?>
						<?php get_template_part( 'parts/loop','archive-grid' ); ?>

					<?php endwhile; ?>

				</div>

			</div>

		</section>

	</aside><!--row-->

</div>

	</div>

	<?php endif; ?>
		
	<?php wp_reset_postdata();
