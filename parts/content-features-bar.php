<?php
/**
 * Sub-navigation and / or breadcrumb bar
 *
 * @package elami
 **/

	$feature_text = get_theme_mod( 'elami_home_feature_text' );
$feature_link = get_theme_mod( 'elami_home_feature_link' );
if ( ! empty( $feature_text ) ) : ?>
	<div class="footer__feature">
		<?php if ( ! empty( $feature_link ) ) : ?>
			<?php esc_html_e( 'About this image: ','elami' ); ?>
			<a class="footer__feature__link" href="<?php echo get_the_permalink( $feature_link ); ?>">
		<?php endif; ?>
			<span class="footer__feature__text">
				<?php echo esc_html( $feature_text ); ?>
			</span>
		<?php if ( ! empty( $feature_link ) ) : ?>
			</a>
		<?php endif; ?>

	</div>
<?php endif; ?>