<div class="social-items">
    <ul class="social-items__list">
        <li class="elami_instagram">
            <a href="https://www.instagram.com/elamiagency" title="Follow élami agency on Instagram">
                <?php echo elami_build_svg( get_stylesheet_directory() . '/assets/images/svg/instagram.svg', true ); ?>
            </a>
        </li>
        <li class="elami_linkedin">
            <a href="https://www.linkedin.com/in/elaine-michon-24061612/" title="Connect with Élaine on Linkedin">
                <?php echo elami_build_svg( get_stylesheet_directory() . '/assets/images/svg/linkedin.svg', true ); ?>
            </a>
        </li>
        <li class="elami_email">
            <a href="<?php elami_the_contact_link( 2, false ); ?>" title="Send Élaine an e-mail">
                <?php echo elami_build_svg( get_stylesheet_directory() . '/assets/images/svg/mail.svg', true ); ?>
            </a>
        </li>
    </ul>
</div>