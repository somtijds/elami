<?php if (have_posts()) : ?>
<div class="section-wrapper">
	<div id="slider">
		<figure>
			<?php the_post_thumbnail('full'); ?>
		</figure>
	</div>
</div><!-- section-wrapper-->
<?php endif; ?>
