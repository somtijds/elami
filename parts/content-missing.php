<div id="post-not-found" class="hentry">
	
	<?php if ( is_search() ) : ?>
		
		<header class="article-header">
			<h1><?php _e( 'Sorry, No Results.', 'elami' );?></h1>
		</header>
		
		<section class="entry-content">
			<p><?php _e( 'Try your search again.', 'elami' );?></p>
		</section>
		
		<section class="search">
		    <p><?php get_search_form(); ?></p>
		</section> <!-- end search section -->
		
		<footer class="article-footer">
			<p><?php _e( 'This is the error message in the parts/content-missing.php template.', 'elami' ); ?></p>
		</footer>
		
	<?php else: ?>
		
		<div class="row">

			<div class="content-wrapper-404 columns medium-8 medium-offset-2">
				<header class="article-header">
					<h1><?php _e( 'Huh? Nothing found!', 'elami' ); ?></h1>
				</header>
				
				<section class="entry-content">
					<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'elami' ); ?></p>
				</section>
				
			</div>

		</div>
			
	<?php endif; ?>
	
</div>
