<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
	
	<div class="article-inner row">
		
		<figure class="columns large-4 medium-6 small-12"><?php the_post_thumbnail('medium'); ?></figure>
		
		<div class="content-wrapper large-8 medium-6 small-12 columns">
			
			<header class="article-header">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<div class="section__header__subtitle subheader">
					<div class="subheader__artist-type"><?php the_terms( get_the_id(), 'artist_type' ); ?></div>
					<div class="subheader__representation"><?php elami_the_representation_subheader(); ?></div>
				</div>
			</header> <!-- end article header -->

            <?php if ( elami_is_under_management() || is_user_logged_in() ) : ?>
			
			<section class="entry-content" itemprop="articleBody">

				<?php the_content('<button class="tiny">' . __( 'Read more...', 'elami' ) . '</button>'); ?>
			</section> <!-- end article section -->

            <?php endif; ?>
			
			<footer class="article-footer">
				<?php if ( elami_has_books() ) : ?>
					<a class="button" href="<?php echo esc_url( trailingslashit( get_post_type_archive_link('book') ) . '?artist_id=' . get_the_ID() ); ?>"><?php _e( 'Show me all the books!','elami' ); ?></a>
				<?php endif; ?>
			</footer> <!-- end article footer -->	
		
		</div>			    						
	
	</div>
</article> <!-- end article -->