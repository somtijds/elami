<!--Item: -->
<div class="<?php elami_the_grid_column_class(); ?> <?php elami_the_last_post_class(); ?>" data-equalizer-watch>
		
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
		<?php if ( has_post_thumbnail() ) : ?>
			<section class="featured-image" itemprop="articleBody">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
				<?php the_post_thumbnail('medium'); ?>
				</a>
			</section> <!-- end article section -->
		<?php else : ?>
			
			<?php 
			global $post;
			if ( 'book' === $post->post_type ) : ?>
				<section class="featured-image placeholder" itemprop="articleBody">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						<svg viewBox="0 0 100 60">
						  <g>
						  	<rect x="25%" y=0 height="100%" width="50%" stroke-width="0.667" stroke="#D63240" fill="none" />
						  	<text x="50%" y="40%" text-anchor="middle"><?php _e( 'Cover','elami' ); ?></text>
						  	<text x="50%" y="55%" text-anchor="middle"><?php _e( 'coming','elami' ); ?></text>
						  	<text x="50%" y="70%" text-anchor="middle"><?php _e( 'soon!','elami' ); ?></text>

					  	  </g>
					  	</svg>
					</a>
				</section> <!-- end article section -->

			<?php endif; ?>

		<?php endif; ?>
			
		<header class="article-header">
			<h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>	
				<?php elami_the_book_artists(); ?>

		</header> <!-- end article header -->

	</article> <!-- end article -->
			
</div>

