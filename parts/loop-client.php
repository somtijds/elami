<?php
/**
 * Client loop template.
 *
 * @package elami
 **/

?>

<div class="client-list__item grid__item" data-equalizer-watch>
						
	<!-- client logo (with archive link) -->
	<?php if ( ! empty( $client['client_image_id'] ) ) : ?>
	<figure class="client-list__item__image grid__item__image">
		<?php
		unset( $client_tax_link );
		$img_src = wp_get_attachment_image_url( $client['client_image_id'], 'medium' );
		$img_srcset = wp_get_attachment_image_srcset( $client['client_image_id'], 'medium' );
		$client_tax = $client['client_taxonomy_select'];
		if ( ! empty( $client_tax )
				&& is_numeric( $client_tax )
				&& in_array( $client_tax, $publisher_term_ids ) ) :

			$client_tax_link = get_term_link( intval( $client_tax ) ); ?>	
			<a href="<?php echo esc_url( $client_tax_link ); ?>">

		<?php endif; ?>
		<img src="<?php echo esc_url( $img_src ); ?>"
			srcset="<?php echo esc_attr( $img_srcset ); ?>"
			sizes="(max-width: 50em) 100vw, 400px" alt="<?php echo esc_attr( $client['client_name'] ); ?>">
		<?php if ( isset( $client_tax_link ) ) : ?>
			</a>
		<?php endif; ?>
	</figure>
	<?php endif; ?>

	<!-- client name -->
	<?php if ( ! empty( $client['client_name'] ) ) : ?>
	<header class="client-list__item__title">
		<h3><?php echo esc_html( $client['client_name'] ); ?></h3>
		<?php if ( ! empty( $client['client_name'] ) ) : ?>
			<span class="client-list__item__subtitle subheader">
				<?php echo esc_html( $client['client_type'] ); ?>
			</span>
		<?php endif; ?>
	</header>
	<?php endif; ?>
	
	<!-- client description -->
	<?php if ( ! empty( $client['client_description'] ) ) : ?>
		<p class="client-list__item__description">
			<?php echo esc_html( $client['client_description'] ); ?>
		</p>
	<?php endif; ?>
	<?php if ( ! empty( $client['client_url'] ) ) : ?>
		<p class="client-list__item__url">
			<a target="_new" href="<?php echo esc_url( $client['client_url'] ); ?>" title= "<?php echo wp_sprintf( __( 'Visit %s online','elami' ), $client['client_name'] ); ?>"><?php echo esc_url( $client['client_url'] ); ?></a>
		</p>
	<?php endif; ?>

	<!-- client archive button -->
	<?php if ( isset( $client_tax_link ) ) : ?>
		<a class="button client-list__item__button" href="<?php echo esc_url( $client_tax_link ); ?>">
			<?php esc_html_e( 'Show me all the books!','elami' ); ?>
		</a>
	<?php endif; ?>
						
</div>
