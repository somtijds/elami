<?php global $wp_query; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

	<header class="article-header">
		<a class="bg-toggler" data-toggle="bg-<?php the_ID(); ?>">
			<h4 class="entry-title single-title" itemprop="headline">
				<?php the_title(); ?>
			</h4>
		</a>
    </header> <!-- end article header -->
	<section id="excerpt-<?php the_id(); ?>" class="article__excerpt" data-toggler=".active">
		<?php the_excerpt(); ?>
	</section>

</article> <!-- end article -->
