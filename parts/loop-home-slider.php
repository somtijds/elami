<?php if ( has_post_thumbnail() ) : ?>
	<?php
		$thumb_id = get_post_thumbnail_id( $post->ID );
		$img_src = wp_get_attachment_image_url( $thumb_id, 'full' );
		global $wp_query;
		$active = $wp_query->current_post === 0 ? ' active' : ''; ?>
	<div id="bg-<?php the_ID(); ?>" <?php post_class('slide' . $active ); ?>
		style="background-image:url(<?php echo esc_attr( $img_src ); ?>);background-size:cover;"
		data-toggler=".active">
	</div>
<?php else : ?>
	<?php var_dump( $post ); ?>
<?php endif; ?>
