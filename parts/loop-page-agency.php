<?php
/**
 * Page loop template.
 *
 * @package elami
 **/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('has-sections'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<div class="section-wrapper">

		<section class="entry-content" itemprop="articleBody">

			<?php if ( has_post_thumbnail() ) : ?>
				<aside class="entry-image section__image">
					<?php echo get_the_post_thumbnail( $post->ID, 'medium_large' ); ?>
				</aside>
			<?php endif; ?>

			<div class="section__text">
				<header class="article-header section__header">
					<h1 class="page-title"><?php the_title(); ?></h1>
					<?php
						$subtitle = get_post_meta( get_the_ID(), 'elami_page_subtitle', true );
						if ( ! empty( $subtitle ) ) : ?>
						<span class="section__header__subtitle subheader">
							<?php echo esc_html( $subtitle ); ?>
						</span>
					<?php endif; ?>
				</header> <!-- end article header -->

				    <?php the_content(); ?>
			
				<footer class="article-footer">

					<?php elami_the_child_page_buttons( __('Read more about our %s','elami' ) ); ?>
		
					<?php elami_the_contact_link( 2 ); ?>

				</footer> <!-- end article footer -->

			</div>

		    <?php /* wp_link_pages(); */ ?>

		</section> <!-- end article section -->

	</div><!-- section-wrapper-->

	<?php
	$clients = get_post_meta( $post->ID, 'elami_group_clients', true );
	if ( ! empty( $clients ) && count( $clients ) ) : ?>

		<div class="section-wrapper">

			<section class="secondary-content">

				<header class="section__header section__header--full">
					<h1><?php esc_html_e( 'Clients', 'elami' ); ?></h1>
				</header> <!-- end article header -->

				<div class="client-list grid" data-equalizer>

				<?php // Get a list of non-empty publisher term ids to show "show me all the books" links inside loop.
				$publisher_term_ids = elami_get_publisher_term_ids();	?>
				<?php foreach ( $clients as $client ) : ?>
					
					<?php include( get_stylesheet_directory() . '/parts/loop-client.php' ); ?>
				
				<?php endforeach; ?>
				</div><!-- grid -->

			</section>

		</div><!--section-wrapper-->

		<?php endif; ?>

</article> <!-- end article -->
