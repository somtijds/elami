<?php
/**
 * Page loop template.
 *
 * @package elami
 **/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
	<header class="elami-usp">
		<h1><?php the_content(); ?></h1>
	</header>
	<?php
		$button_one_link = get_theme_mod( 'elami_home_button_one_link' );
		$button_one_text = get_theme_mod( 'elami_home_button_one_text' );
		$button_two_link = get_theme_mod( 'elami_home_button_two_link' );
		$button_two_text = get_theme_mod( 'elami_home_button_two_text' );
		$button_one = ! empty( $button_one_link ) && ! empty( $button_one_text );
		$button_two = ! empty( $button_two_link ) && ! empty( $button_two_text );
		if ( $button_one || $button_two ) : ?>
			<section class="call-to-action">
			<?php if ( $button_one ) : ?>
				<a class="button large" href="<?php echo esc_url( get_permalink( $button_one_link ) ); ?>"><?php echo esc_html( $button_one_text ); ?></a>
			<?php endif; ?>
			<?php if ( $button_two ) : ?>
				<a class="button large" href="<?php echo esc_url( get_permalink( $button_two_link ) ); ?>"><?php echo esc_html( $button_two_text ); ?></a>
			<?php endif; ?>

			</section>
		<?php endif; ?>
</article> <!-- end article -->
