<?php
/**
 * Page loop template.
 *
 * @package elami
 **/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('has-sections'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<div class="section-wrapper">

		<section class="entry-content" itemprop="articleBody">

			<?php if ( has_post_thumbnail() ) : ?>
				<aside class="entry-image section__image">
					<?php echo get_the_post_thumbnail( $post->ID, 'medium_large' ); ?>
				</aside>
			<?php endif; ?>

			<div class="section__text">
				<header class="article-header section__header">
					<h1 class="page-title"><?php the_title(); ?></h1>
					<?php
						$subtitle = get_post_meta( get_the_ID(), 'elami_page_subtitle', true );
						if ( ! empty( $subtitle ) ) : ?>
						<span class="section__header__subtitle subheader">
							<?php echo esc_html( $subtitle ); ?>
						</span>
					<?php endif; ?>
				</header> <!-- end article header -->

				<?php the_content(); ?>	

				<footer class="article-footer">
				
					<?php elami_the_contact_link( 2 ); ?>

				</footer> <!-- end article footer -->

			</div>
		
		</section> <!-- end article section -->

	</div><!-- section-wrapper-->

	<?php

	$managed_artists_args = array(
		'post_type' => 'artist',
		'post_status' => 'publish',
		'orderby' => 'title',
		'order' => 'ASC',
		'posts_per_page' => 12,
		'meta_query' => array(
			array(
				'key' => 'elami_artist_management',
				'value' => 'on',
			)
		)
	);

	$managed_artists_query = new WP_Query( $managed_artists_args );

	if ( $managed_artists_query->have_posts() ) : ?>

	<div class="section-wrapper">

		<section class="secondary-content">

			<header class="section__header section__header--full">
				<h1><?php esc_html_e( 'Managed artists', 'elami' ); ?></h1>
			</header> <!-- end article header -->
			
			<div class="client-list grid">

			<?php while ( $managed_artists_query->have_posts() ) : ?>
				<?php $managed_artists_query->the_post(); ?>
					
				<?php include( get_stylesheet_directory() . '/parts/loop-archive-artist.php' ); ?>
				
			<?php endwhile; ?>
		
			</div><!-- grid -->

		</section>

	</div><!--section-wrapper-->

	<?php endif; ?>

</article> <!-- end article -->
