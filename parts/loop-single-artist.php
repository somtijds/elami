

<article id="post-<?php the_ID(); ?>" <?php post_class( 'row' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

	<figure class="columns medium-6 small-12"><?php the_post_thumbnail('large'); ?></figure>
				
	<div class="content-wrapper medium-6 small-12 columns">
		
		<header class="article-header section__header">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php elami_the_representation_subheader(); ?>
		</header> <!-- end article header -->

		<section class="entry-content" itemprop="articleBody">

			<?php the_content('<button class="tiny">' . __( 'Read more...', 'elami' ) . '</button>'); ?>
		</section> <!-- end article section -->
						
		<footer class="article-footer">
			<?php if ( elami_has_books() ) : ?>
				<a class="button" href="<?php echo esc_url( trailingslashit( get_post_type_archive_link('book') ) . '?artist_id=' . get_the_ID() ); ?>"><?php _e( 'Show me all the books!','elami' ); ?></a>
			<?php endif; ?>
		</footer> <!-- end article footer -->	
	</div>

</article> <!-- end article -->