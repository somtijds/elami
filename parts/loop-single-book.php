
<div id="inner-content" class="">

	<main id="main" class="" role="main">

		<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								
		<div class="content-wrapper medium-6 small-12 columns">


		<?php if ( has_term( 'picture-book', 'format' ) ) : ?>

			<?php get_template_part( 'parts/content', 'book-slider' ); ?>

		<?php endif; ?>
			
			<header class="article-header section__header">
					<h1 class="page-title"><?php the_title(); ?></h1>
					<div class="section__header__subtitle subheader">
						<?php elami_the_book_artists(); ?>
					</div>
			</header> <!-- end article header -->

			<section class="entry-content" itemprop="articleBody">

				<?php the_content('<button class="tiny">' . __( 'Read more...', 'elami' ) . '</button>'); ?>

			</section> <!-- end article section -->

			<?php elami_the_book_details(); ?>

			<footer class="article-footer">
				
				<?php elami_the_contact_link( 2 ); ?>

			</footer> <!-- end article footer -->

		</div>			    

		<figure class="columns medium-6 small-12"><?php the_post_thumbnail('large'); ?></figure>
		
		</article> <!-- end article -->

		<?php $series = wp_get_post_terms( get_the_ID(), 'series' ); ?>

		<?php if ( ! empty( $series ) ) : ?>

			<?php include_once( get_stylesheet_directory() . '/parts/content-book-series.php' ); ?>

		<?php endif; ?>

	</main>

</div>