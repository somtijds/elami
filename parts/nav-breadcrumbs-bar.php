<?php
/**
 * Sub-navigation and / or breadcrumb bar
 *
 * @package elami
 **/

 ?>
<div class="breadcrumbs-bar">

	<?php $parent_id = wp_get_post_parent_id( get_the_ID() );
	if ( is_singular( 'page' ) ) : ?>

		<div class="button-group">

		<?php if ( ! empty( $parent_id ) && is_numeric( $parent_id ) ) : ?>

			<?php $parent = get_post( $parent_id );

			if ( $parent instanceof WP_Post ) : ?>

				<a class="button" href="<?php echo esc_url( get_the_permalink( $parent->ID ) ); ?>" title="<?php echo esc_attr( wp_sprintf( __( 'Navigate to %s','elami' ), $parent->post_title ) ); ?>"><?php echo esc_html( $parent->post_title ); ?></a>

			<?php endif; ?>
			
		<?php endif; ?>

			<a class="button active" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		    <?php elami_the_child_page_buttons(); ?>

		</div>

	<?php else : ?>

		<?php if ( function_exists( 'breadcrumb_trail' ) ) : ?>

			<?php $breadcrumb_args = array(
				'container'       => 'nav',
				'before'          => '',
				'after'           => '',
				'show_on_front'   => true,
				'network'         => false,
				'show_title'      => true,
				'show_browse'     => false,
				'echo'            => true,
				// 'post_taxonomy' => array(
				//     'book'  => 'publisher', // 'post' post type and 'publisher' taxonomy
				//     'book'  => 'genre',    // 'book' post type and 'genre' taxonomy
				//     'book'  => 'series',
				// ),
			);
			echo breadcrumb_trail( $breadcrumb_args ); ?>

		<?php endif; ?>

	<?php endif; ?>

	<?php 

		global $wp_query;

		if ( ! isset( $wp_query->query['artist_id'] ) && ( is_post_type_archive( array( 'book', 'artist' ) ) || is_tax( array( 'format', 'artist_type' ) ) ) ) : ?>

		<?php $query_object = $wp_query->get_queried_object();

		if ( empty( $query_object->taxonomy ) ) {
            $taxo = 'book' === $query_object->name ? ['format', 'available_rights'] : ['artist_type'];
		} else {
			$taxo = array( $query_object->taxonomy );
		}

        elami_the_taxonomy_buttons( $taxo );

        endif; ?>

</div>