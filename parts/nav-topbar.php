<div class="top-bar" id="main-menu">
	<div class="top-bar-left">
		<ul class="menu">
			<li class="menu-text">
				<a href="<?php echo home_url(); ?>">
					<?php echo elami_build_svg( get_stylesheet_directory() . '/assets/images/svg/elami-menu-logo.svg', true ); ?>
					<span class="show-for-sr"><?php bloginfo('name'); ?></span>
				</a>
			</li>
		</ul>
		<ul class="menu togglers">
		<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
		<?php if ( is_plugin_active('polylang') ) : ?>
			<li class="language-menu-toggle menu-toggle" data-responsive-toggle="menu-mobile-language-menu" data-hide-for="medium" >
				<a data-toggle><?php _e('LANG','elami'); ?></a>
			</li>
		<?php endif; ?>
			<li class="main-menu-toggle menu-toggle" data-responsive-toggle="menu-main-menu" data-hide-for="medium" >
				<a data-toggle><?php _e('MENU','elami'); ?></a>
			</li>
		</ul>
	</div>
	<div class="top-bar-center">
		<?php elami_top_nav(); ?>
		<?php elami_mobile_language_nav(); ?>	
	</div>
	<div class="top-bar-right">
		<?php elami_language_nav(); ?>
	</div>
</div>
