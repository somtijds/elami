<?php get_header(); ?>
			
<main id="content" role="main">
		
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part( 'parts/loop', 'single-artist' ); ?>
		
	<?php endwhile; else : ?>

			<?php get_template_part( 'parts/content', 'missing' ); ?>

	<?php endif; ?>

</main> <!-- end #content -->

<?php get_footer(); ?>