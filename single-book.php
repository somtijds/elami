<?php
/*
Single Book Template
*/
?>

<?php get_header(); ?>
			
<main id="content" role="main">
		
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php get_template_part( 'parts/loop', 'single-book' ); ?>

	<?php endwhile; else : ?>

			<?php get_template_part( 'parts/content', 'missing' ); ?>

	<?php endif; ?>

</main> <!-- end #main -->

<?php get_footer(); ?>