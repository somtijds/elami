<?php
/*
Template Name: Full Width (No Sidebar)
*/
?>

<?php get_header(); ?>
			
<main id="content" class="large-12 medium-12 columns" role="main">
				
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part( 'parts/loop', 'page' ); ?>
		
	<?php endwhile; endif; ?>							

</main> <!-- end #content -->

<?php get_footer(); ?>
